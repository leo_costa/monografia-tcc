# Monografia do Projeto

[![Ver Monografia](https://img.shields.io/badge/Ver-Monografia-blue)](https://leo_costa.gitlab.io/monografia-tcc/tcc_sapt.pdf)
[![Link para o editor](https://img.shields.io/badge/Editar-Monografia-blue)](https://www.papeeria.com/p/efdd7beab51b04fc2e6cbe0a445fc946#/tcc_sapt.tex)

## :warning: Importante

Ao editar a monografia no editor online, **SEMPRE** certifique-se de que os
arquivos estão sincronizados com o repositório.

Para fazer isso siga estes passos:

* Selecione a aba de _Version Control_, clique na opçào _Git_ e clique na opção
  _Sync now_: <img src="./gitmono.png"  width="400">

* Após isso irá aparecer uma outra janela, basta clicar no botão _Sync_

* Quando o processo terminar deve estar tudo sincronizado e pronto para ser
  editado.

