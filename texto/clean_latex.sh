#!/usr/bin/sh
rm *.xmpi
rm *.aux
rm *.bbl
rm *.bcf
rm *.blg
rm *.idx
rm *.lof
rm *.log
rm *.lot
rm *.mw
rm *.out
rm *.pdf
rm *.run.xml
rm *.synctex.gz
rm *.toc
rm *.xdy
rm *.alg
rm *.fdb_latexmk
rm *.fls
rm *.ilg
rm *.ind

rm ./pre-textuais/*.aux
rm ./textuais/*.aux
