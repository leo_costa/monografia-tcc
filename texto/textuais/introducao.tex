\chapter{Introdução}\label{sec:introTCC}

O processo de triagem e classificação de risco é um passo importante na
administração dos recursos médicos no serviço de emergência de um hospital, ou
seja, é através dessa avaliação que é possível direcionar de maneira efetiva os
recursos médicos, que na maioria das vezes são escassos, para o atendimento dos
pacientes. Existem diversas formas de realizar esta classificação de risco,
porém, dados os estudos científicos, o \glsxtrlong{stm}\footnotemark
(\glsxtrshort{stm}) é caracterizado como um sistema válido e confiável
\cite{coutinho2012classificaccao}.

\footnotetext{Também denominado de Sistema Manchester de Classificação de Risco
(SMCR).}

De acordo com \textcite{coutinho2012classificaccao}, o \glsxtrshort{stm} foi
implementado pela primeira vez no Brasil no ano de $2008$, em Minas Gerais.
Desde então, o sistema também foi adotado por outros hospitais, como, o
\glsxtrlong{hu} (\glsxtrshort{hu}).

Como pode ser visto em \textcite{cicoloavaliaccao}, o \glsxtrshort{stm} começou
a ser utilizado no \glsxtrshort{hu} por volta do ano de $2012$, e, atualmente,
o processo de triagem é quase que totalmente informatizado, uma vez que,
utiliza-se um dispositivo eletrônico que consulta os fluxogramas descritos no
protocolo e os sinais vitais do paciente para realizar a classificação. Alguns
detalhes do \glsxtrshort{stm} serão explicados na Seção \ref{sec:manchester}.

Embora o processo de triagem seja computadorizado, o gerenciamento da
classificação atribuída ao paciente não é. Ou seja, após o processo de triagem,
o gerenciamento dos pacientes é feito utilizando fichas de papel e, dessa
forma, não há possibilidade de uma fácil alteração na classificação do paciente
caso uma reavaliação seja feita.

\section{Justificativa}

Embora o \glsxtrshort{stm} seja eficiente para classificar o paciente, podem
ocorrer alguns casos em que o quadro clínico do mesmo pode piorar drasticamente
durante o tempo de espera. Um exemplo dado pelo Prof. Dr. Oscar Fugita
(\glsxtrshort{hu}) é, um paciente no início de uma apendicite aguda pode ser
classificado como amarelo ou até mesmo verde, que são classificações que
permitem um tempo de atendimento de algumas dezenas de minutos, porém, enquanto
na sala de espera, o paciente pode ter uma grande piora no seu estado clínico,
de forma a ser necessária uma cirurgia quase que imediatamente. Casos como o do
exemplo dado são difíceis de serem previstos no processo de triagem, a não ser
que o ``olho clínico'' do profissional de triagem seja capaz de detectar isto.

Além disso, em \textcite{livroProtocoloManchester}, onde é descrito o protocolo
de Manchester e todas suas nuances, é destacado que o processo de triagem deve
ser algo dinâmico, dadas as mudanças que podem acontecer no quadro clínico do
paciente após a triagem inicial, assim, tornando o acompanhamento necessário.

Portanto, fica clara a necessidade de um certo acompanhamento dos pacientes na
sala de espera, porém, não é viável possuir um profissional treinado para
simplesmente ficar observando as pessoas neste ambiente, um profissional deste
tipo possui uma utilidade muito maior atendendo os pacientes. Dessa forma, um
sistema inteligente que seja capaz de realizar este monitoramento é
muito mais útil e com um custo-benefício melhor para os hospitais.

\section{Objetivo}\label{sec:objetivo}

Com esses motivos em mente, o objetivo principal é desenvolver um sistema de
monitoramento dos pacientes pós-triagem que seja capaz de perceber o agravamento
no quadro clínico dos pacientes na sala de espera e realizar ajustes na
classificação de risco do mesmo.

%O projeto teve como local de estudo a sala de espera da ala de emergência do
%\glsxtrshort{hu}. Vale a pena salientar que, no hospital, só são tratados
%pacientes de classificação amarela ou superior, os outros que possuem uma
%classificação de risco menor (verde e azul), são direcionados para
%tratamento em Unidades de Pronto Atendimento (UPA) ou unidades de Assistência
%Médica Ambulatorial (AMA).

Para realizar esta tarefa, são definidos os seguintes objetivos específicos:

\begin{itemize}[label=$\bullet$]
  \item \glsxtrlong{nath} - \glsxtrshort{nath};
  \item Sistema Digital de Atendimento.
\end{itemize}

A \glsxtrshort{nath} é um sistema de monitoramento capaz de alterar a
classificação dos pacientes com base em alterações do quadro clínico do
paciente. Utilizando modelos de redes neurais, o sistema é capaz de detectar
alterações no nível de dor do paciente, dessa forma, ao perceber uma possível
piora, uma abordagem interrogativa é realizada utilizando fluxogramas baseados
no \glsxtrshort{stm}. O sistema \glsxtrshort{nath} foi criado para ser
embarcado em um robô móvel, porém, o robô móvel não faz parte do escopo deste
projeto.

Para execução de suas atividades, a \glsxtrshort{nath} conta com sistemas
auxiliares como: reconhecimento facial, sistema de câmeras para localização,
sistema \textit{Text-to-Speech} e \textit{Speech-to-Text} e o sistema de
reconhecimento das expressões de dor. Além de ter a possibilidade para quando
embarcada aferir temperatura e pulsação dos pacientes como parâmetros de
avaliação.

A partir de conversas com o Prof. Dr. Oscar Fugita, ficou definido que a
detecção da intensidade da dor e do comportamento do paciente são indicadores
comumente utilizado pelos médicos para detectar se o quadro clínico do paciente
piorou. Apesar de abordar neste projeto o comportamento como um discriminante e
um possível gatilho para o sistema, escolheu-se utilizar apenas a detecção de
dor para a implementação, deixando a análise de comportamento como um futuro
passo a ser dado para melhoria do sistema.

Além disso, a \figura{fig:wcProt} demonstra uma análise dos fluxogramas do
\glsxtrshort{stm}. Mais especificamente, o estudo foi feito com base nos
discriminantes responsáveis por determinar uma classificação de nível laranja
ou vermelho. Nesta figura é possível notar que uma alta intensidade de dor
(\textit{severe pain}) é um discriminante muito comum no \glsxtrshort{stm},
portanto, é válido que este discriminante seja monitorado pelo sistema.

Contudo, para este projeto serão utilizados apenas os fluxogramas de dor
abdominal, torácica e lombar do \glsxtrshort{stm}. Tais fluxogramas foram
escolhidos por possuir maior conexão com o aumento do nível de dor, segundo os
enfermeiros e o Dr. Oscar Fugita em reunião realizada com o corpo técnico do
\glsxtrshort{hu}.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{.9\textwidth} % Regula o tamanho da figura
            \centering
                \caption{Nuvem de palavras para os discriminantes de nível
                laranja e vermelho}\label{fig:wcProt}
                \includegraphics[width=\linewidth]{Imagens/wcDiscriminantesVL.eps}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}


O Sistema Digital de Atendimento é o segundo componente do projeto. Esse
sistema foi projetado devido à atual falta de um sistema digital de fichas no
\glsxtrshort{hu}. Sem um sistema digital é impossível realizar alterações
na classificação dos pacientes. Aproveitando essa lacuna, o sistema foi
projetado para realizar a chamada de atendimento, ou seja, além de atualizar a
classificação dos pacientes, ele também é responsável por chamar os
pacientes na sala de espera e indicar à qual sala médica os mesmos devem se
dirigir.

Esse sistema conta com suas telas principais funcionando em três locais do
hospital: sala de espera, sala de triagem e sala médica. Na sala de triagem, a
enfermeira é responsável por cadastrar o paciente no sistema, inserindo os
dados na tela de cadastro. A partir da tela auxiliar, na sala médica, o médico
é responsável por chamar o próximo paciente a ser atendido, utilizando a tela
de chamada médica, ou, reinserir um paciente na lista de espera, caso tenha
perdido a primeira chamada, ou necessita de um retorno após a medicação. Por fim,
na sala de espera, tem-se a tela de chamada, onde os pacientes serão
notificados de sua chamada e qual a sala médica de atendimento.

Ao atender um paciente na sala de triagem, a enfermeira responsável coloca
alguns dados no sistema, entre eles, a classificação atual do paciente. O
paciente, ao permanecer na sala de espera, é monitorado pela
\glsxtrshort{nath} e, caso tenha alteração em seu quadro clínico, tem a sua
classificação atualizada. O médico é responsável por chamar o próximo paciente
em sua tela no Sistema Digital de Atendimento e, automaticamente, a tela na
sala de espera alerta ao paciente.

Na revisão bibliográfica foram estudadas as técnicas necessárias para que o
sistema funcione corretamente, ou seja, para que ele possa reconhecer a dor,
comportamento e face do paciente, localizá-lo na sala de espera, e, também, ser
capaz de entender algumas perguntas simples que venham a ser necessárias para a
reclassificação do mesmo.
