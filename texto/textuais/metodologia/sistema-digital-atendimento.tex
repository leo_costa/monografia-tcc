 \section{Sistema Digital de Atendimento}\label{sec:SDA}

O Sistema Digital de Atendimento foi projetado para suprir a falta de um
sistema digital de fichas no \glsxtrshort{hu}. Como já ressaltado, sem um
sistema digital é impossível realizar alterações na classificação dos
pacientes.

Aproveitando tal lacuna, incluiu-se ao sistema a realização das chamadas de
atendimento, ou seja, além de atualizar a classificação dos pacientes, ele
também é responsável por chamar os pacientes na sala de espera e indicar
para qual sala médica os mesmos devem se dirigir.

O sistema conta com quatro telas principais voltadas para atendimento dos
pacientes e uma quinta tela reservada para apresentação de indicadores de
gerenciamento do atendimento fornecido pelo hospital. Com isso, as cinco telas
do sistema são: cadastro, chamada médica, chamada de reavaliação, chamada dos
pacientes e \textit{dashboard}.

Para acessar cada uma das telas, projetou-se um determinado tipo de usuário,
impedindo acessos indevidos e delimitando escopos de trabalho. Os tipos de
usuário são: administrador, médico e enfermeiro. Sendo que o administrador do
sistema é responsável por cadastrar novos usuários, além disso, ele também
possui acesso a todas funcionalidades do sistema.

\subsection{Desenvolvimento do Sistema}

O sistema é constituído de duas camadas principais conhecidas como
\textit{front-end} e \textit{back-end}. Essa divisão permite a construção de
arquiteturas diferentes para cada camada, facilitando uma escalabilidade do
projeto. A \figura{fig:arquiteturaSistema} ilustra essa divisão.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{0.6\textwidth} % Regula o tamanho da figura
                \caption{Arquitetura do Sistema Digital de Atendimento.}
                \label{fig:arquiteturaSistema}
                 \includegraphics[width=\linewidth]{Imagens/Backend.pdf}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}

O \textit{front-end} é a parte visível do sistema e pode ser manipulado pelo
usuário, ele possibilita a interação do usuário com os recursos do sistema de
uma forma amigável, além disso, é o responsável por fazer as requisições ao
\textit{back-end}.

O \textit{back-end} é a parte do sistema que o usuário não vê, ele é o
responsável por fazer as requisições do \textit{front-end}  ao banco de dados,
garantindo as regras de negócio, como, o login na plataforma, a segurança, no
sentido de garantir que cada usuário possua um \textit{token} e senha
criptografada, e por último, possibilita a escalabilidade do projeto. Já o
banco de dados é o local onde são armazenados os dados no formato de tabelas,
mantendo a integridade dos dados para utilizações futuras pela
\glsxtrshort{nath}. A seguir será melhor detalhada a construção das duas
camadas e as tecnologias utilizadas para elaboração das mesmas.

\subsection{Construção do \textit{back-end}}

O primeiro passo para construção do \textit{back-end} é a estruturação do banco
de dados para atender às regras de negócio. Por isso, foram criadas 3 tabelas
utilizando a ferramenta DbDesigner para criar o Diagrama Entidade Relacional
(DER) ilustrado na \figura{fig:DER}, onde é possível ver os atributos, seus
respectivos tipos e os relacionamentos entre as tabelas.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{\textwidth} % Regula o tamanho da figura
                \caption{Diagrama Entidade Relacional do Sistema Digital de Atendimento.}
                \label{fig:DER}
                 \includegraphics[width=\linewidth]{Imagens/sql_db_rel.png}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}

Como pode ser visto na  \figura{fig:DER}, as 3 tabelas utilizadas no sistema
são: usuários, pacientes e informações. Na tabela de usuários, são armazenados
os dados dos usuários que utilizarão o sistema. Já na tabela de pacientes, são
armazenado os dados dos pacientes do hospital e a tabela de informações foi
criada para armazenar as estatísticas diárias do sistema que serão
disponibilizadas na tela \textit{dashboard}. Além disso, é notável que existe
um relacionamento entre as tabelas de usuário e paciente, essa relação é
denominada como um relacionamento de um para muitos, ou seja, cada paciente
pode ser atendido por um usuário e um usuário pode atender vários pacientes
naquela determinada data. Essa conexão possibilita identificar qual médico
está realizando o atendimento do paciente, facilitando uma consulta aos dados.

Essas tabelas são guardadas no banco de dados MySQL, que é um sistema de
gerenciamento de banco de dados relacional e que utiliza a linguagem SQL. Esse
sistema permite trabalhar com um banco de dados local ou acessar bancos de
dados que são armazenados em nuvem. Para possibilitar uma integração do Sistema
Digital de Atendimento com a \glsxtrshort{nath}, no sentido de compartilhar os
dados cadastrados, optou-se por trabalhar com um banco de dados em nuvem, pela
facilidade de acessar os dados online. Assim, foi utilizada a Google Cloud
Platform para gerenciar  e compartilhar entre os sistemas o banco de dados
deste projeto.

Em seguida, para fazer as configurações essenciais para o \textit{back-end}
atuar como uma API REST \footnotemark, foi utilizado o \textit{framework}
Spring Boot.  O emprego de uma API REST possibilita construir uma interface que
fornece dados em um formato padronizado baseado em requisições
HTTP\footnotemark, essa característica facilita a comunicação com o
\textit{front-end}.

\footnotetext{O acrônimo API refere-se ao termo \textit{Application Programming
Interface} (Interface de Programação de Aplicações). Já a sigla REST provém do
termo \textit{Representational state transfer}(Transferência de Estado
Representacional).  Para saber mais sobre, vide \textcite{jacksonpires2017}.}

\footnotetext{A sigla HTTP advém de \textit{HyperText Transfer Protocol} e é
denominado como um protocolo de comunicação. Para saber mais sobre, vide
\textcite{ivandesouza2019}.}

O uso do Spring Boot permite otimizar e aumentar a produtividade, por isso, ele
fornece um site\footnotemark  para criar o projeto com algumas configurações
iniciais, como, escolher a linguagem de desenvolvimento, versão do Spring Boot,
dependências para auxiliar no desenvolvimento e outras definições. Dessa forma,
neste projeto foi configurado para utilizar o Maven como gerenciador de
pacotes, versão do Spring 2.4.1, algumas definições de nome do projeto, versão
do Java 11 e as dependências principais, como, Spring Web, Spring Boot
DevTools, Spring Data JPA, Spring Boot Security e MySQL Driver.

\footnotetext{Site para gerar o projeto inicial do \textit{back-end}:
\url{https://start.spring.io}}

Em seguida, iniciou-se o processo de desenvolvimento do programa em Java e,
para isso, utilizou-se o Eclipse como ambiente de desenvolvimento. Para a
aplicação conseguir acessar o banco de dados e fazer as manipulações dos dados,
é necessário fazer a configuração adicionando alguns comandos no arquivo
\textit{application.properties} localizado no diretório
\textit{src/main/resources}. Neste arquivo são adicionados os comandos para
estabelecer a conexão do banco de dados, passando o usuário e senha do banco de
dados, autorização de operações e a possibilidade de visualizar as operações no
console.

O próximo passo é criar as outras classes da  API e, para isso, foram criados
novos pacotes: \textit{Entities}, \textit{Repository}, \textit{Service},
\textit{Controller} e a \textit{Config}. Essa divisão oferece uma melhor
organização e, também, uma manutenção melhor da  API. A seguir, é feita uma
breve descrição da responsabilidade de cada camada.

\begin{itemize}[label=$\bullet$]
  \item \textit{Entities}: Ficam as classes que servirão como modelo para
criação das tabelas no banco de dados.
  \item \textit{Repository}: Ficam as classes responsáveis por fazer as
transações com o banco de dados.
  \item \textit{Service}: Ficam as classes responsáveis pelas regras de
negócio, além disso, faz a conexão entre o Controller e o
Repository.
  \item \textit{Controller}: Ficam as classes que fazem a comunicação
com o usuário (\textit{front-end}) através de requisições HTTP.
  \item \textit{Config}: Ficam as configurações de segurança do sistema.
\end{itemize}

 Com a implementação de todas as camadas, é possível fazer a simulação do
\textit{back-end} e, em seguida,  testar todas as funcionalidades criadas antes
mesmo da construção do \textit{front-end}. Neste projeto, tal tarefa foi
realizada com a utilização da ferramenta Postman.

\subsection{Construção do \textit{front-end}}

O \textit{front-end} foi construido para se comportar como uma
SPA (\textit{Single Page Applications}), ou seja, uma aplicação Web que se
comporta como uma aplicativo de \textit{desktop}, passando a sensação de página
única. Essa funcionalidade possibilita uma experiência de usuário melhor, pois,
não é necessário recarregar toda a página ou redirecionar o usuário para uma
página nova, é preciso apenas atualizar de forma assíncrona o conteúdo
principal, permanecendo toda a estrutura da página estática.

Para implementação dessa camada foi utilizado o JavaScript, que é uma linguagem
de programação interpretada e estruturada, ela é executada pelo navegador, ou
seja, no \textit{client-side}, isso quer dizer que o seu processamento é feito
pelo \textit{browser} do computador. Ela é utilizada para deixar a página com
mais movimento, podendo atualizar elementos dinamicamente e lidar melhor com
dados enviados e recebidos na página.

Além disso, foi necessário o emprego do HTML (\textit{Hyper Text Markup
Language}), caracterizada como uma linguagem de marcação de hipertexto,
utilizada para estruturar os elementos da página, como parágrafos, links,
títulos, tabelas e imagens. Por fim, foi utilizado o CSS (\textit{Cascading
Style Sheets}) que é uma linguagem de estilização de elementos HTML utilizado
para cores, fontes e posicionamento dos elementos na página.

Com o objetivo de criar as interfaces visuais do sistema de maneira flexível e
sobretudo eficiente, foi utilizado uma biblioteca do JavaScript denominada como
React. A sua utilização facilita o processo de criação das páginas do sistema,
pois, usa o conceito de utilização de componentes, ou seja, cada parte da
página é criado como um componente de modo que possa ser reutilizado, assim,
aumentando a produtividade de desenvolvimento do \textit{front-end}.

Assim, após a estruturação de todas as telas do Sistema Digital de Atendimento,
apresentadas detalhadamente na Seção \ref{sec:res-sist-digital}, foi possível
fazer a integração das duas camadas, verificando se todas as requisições do
\textit{front-end} são atendidas no \textit{back-end}.
