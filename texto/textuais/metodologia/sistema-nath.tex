\section{Sistema NATH}

Como descrito anteriormente, a \glsxtrshort{nath} é um sistema de monitoramento
capaz de alterar a classificação dos pacientes com base em alterações do quadro
clinico do paciente.

O sistema, que foi projetado para ser embarcado em um robô móvel, aborda
pessoas presentes na sala de espera, diferenciando os pacientes dos
acompanhantes, e cadastrando-os em seu banco de pacientes. Após cadastro, o
sistema entra em uma rotina de monitoramento dos pacientes, averiguando
possíveis mudanças nas expressões faciais e relacionando-as a um possível
aumento no nível de dor. Ao detectar tal aumento, o sistema realiza uma nova
abordagem realizando uma série de perguntas baseadas no fluxograma do
\glsxtrshort{stm} que foi utilizado para classificação do paciente. Caso as
novas informações cedidas pelo paciente levem o sistema a entender a
necessidade de uma reclassificação, o mesmo executa tal tarefa, priorizando
aquele atendimento.

Devido aos algoritmos que devem ser utilizados para realizar as tarefas
definidas, foi escolhida a linguagem Python para implementar o sistema
\glsxtrshort{nath}.  Python é uma das linguagens do momento, principalmente na
área de inteligência artificial, além disso, ela também é multiplataforma,
facilitando uma possível implementação embarcada do sistema.

O diagrama de classes do sistema completo pode ser visto no Anexo A.

Nas seções a seguir serão fornecidos os detalhes à respeito da implementação
de cada sub sistema da \glsxtrshort{nath}.

\subsection{Reconhecimento de Dor}

O reconhecimento de dor é o sub sistema principal da \glsxtrshort{nath}, pois é
ele que dá início à reclassificação do paciente.

Para a implementação do modelo de reconhecimento de dor foi utilizado o
\textit{dataset} UNBC-McMaster, cujo acesso foi solicitado pelos integrantes do
grupo.

Na Seção \ref{sec:revisao-bibliografica} foram estudadas diversas formas para
implementar o modelo de detecção de intensidade de dor a partir das expressões
faciais, e, após alguns testes iniciais, decidiu-se por utilizar três redes
\glsxtrshort{mlp}, cada uma treinada com um conjunto de parâmetros.  Esses
parâmetros são: pontos fiduciais da face (PTS), \glsxtrshort{lbp} e os 500
primeiros coeficientes da \glsxtrshort{dct}.

\subsubsection{Pré-processamento}

Antes que estes parâmetros possam ser extraídos da imagem do paciente, é
necessário realizar o pré-processamento das imagens. Isso inclui, detecção
facial, correção de rotação da face, extração do fundo da imagem e normalização
do tamanho da imagem.

Para a detecção facial foram utilizados dois tipos de modelo, ambos pré-treinados.
O primeiro, baseado nos Histogramas de Gradientes Orientados
(\glsxtrlong{hog}--\glsxtrshort{hog}), e o segundo, baseado em redes
convolucionais.  O modelo \glsxtrshort{hog} possui um baixo tempo
computacional, em contrapartida, possui baixa robustez. Já o modelo
convolucional possui um elevado tempo computacional, no entanto, é mais
robusto.  Portanto, caso ocorra falha utilizando o primeiro modelo, o segundo é
utilizado. Isto é importante, pois o modelo \glsxtrshort{hog} não é capaz de
detectar faces caso elas estejam muito rotacionadas, e, existem casos deste
tipo no \textit{dataset}.

A correção de rotação é importante independentemente do tipo de modelo de
reconhecimento de dor, pois a rede neural identifica os padrões numa certa
ordem, e, caso ocorra uma variação desses padrões devido à rotação da face, os
resultados serão impactados. Para implementar a correção de rotação foram
utilizados os dados dos pontos fiduciais da face em conjunto com algumas
funções da biblioteca OpenCV \footnotemark, que possui diversas ferramentas para
lidar com processamento de imagens. Uma das formas de implementar essa correção
de rotação é, utilizar uma Triangulação de Delaunay para mapear as regiões
rotacionadas da face para uma pose padrão, isto é, as regiões dos triângulos da
face rotacionada são mapeadas para as regiões de uma triangulação feita numa
face sem rotações.

\footnotetext{Disponível em: \url{https://opencv.org}.}

Em resumo, a Triangulação de Delaunay é responsável por, a partir de um
conjunto $\mathcal{P}$ de pontos, gerar um conjunto de triângulos de forma que
nenhum ponto do conjunto $\mathcal{P}$ esteja dentro dos círculos circunscritos
dos triângulos, um exemplo disso pode ser visto na \figura{fig:delaunay}. Na
figura é possível ver que nenhum dos pontos está dentro dos círculos.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.5\textwidth} % Regula o tamanho da figura
        \caption{Triangulação de Delaunay.}\label{fig:delaunay}
        \includegraphics[width=\linewidth]{Imagens/delaunay_triang}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Um exemplo do resultado obtido com a correção de rotação pode ser visto na
\figura{fig:corr_rot}, nela, a imagem à esquerda possui uma grande inclinação
para a esquerda, que, após o pré-processamento é removida completamente, como
pode ser visto na imagem à direita.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.55\textwidth} % Regula o tamanho da figura
        \caption{Correção da rotação frontal da face.}\label{fig:corr_rot}
        \includegraphics[width=\linewidth]{Imagens/corr_rot}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Assim como a correção de rotação, a extração do fundo também é feita utilizando
os pontos fiduciais da face, porém, esse passo requer um processamento mais
simples, basta extrair a imagem contida na região delimitada pelos pontos
fiduciais mais externos, aqueles que contornam a face (ver
\figura{fig:pts_face}).

Por fim, a normalização serve para manter as imagens de entrada num tamanho
padrão, que no caso é de $200\times200$ \textit{pixels}. A \figura{fig:pts_face} é um
exemplo de uma imagem que passou por todo o processo de pré-processamento.

\subsubsection{Extração dos Parâmetros}

O parâmetro mais simples de ser extraído é o conjunto de pontos fiduciais
(PTS), cuja extração é feita através de um modelo pré-treinado em conjunto com
as funções da OpenCV. O formato do vetor de características deste
parâmetro por ser visto na \equacao{eq:pts_vec}.

\begin{equation}
  \text{PTS} = \left[ x_1,y_1,x_2,y_2, \cdots x_n,y_n \right], n \in \left[ 1, 64 \right]
  \label{eq:pts_vec}
\end{equation}

A seguir, tem-se a extração dos coeficientes da \glsxtrshort{dct}, que consiste
em calcular a \glsxtrshort{dct} da imagem pós-processada em preto e branco.
Porém, dado o tamanho da imagem, este processo gera $200\times200$
coeficientes, que é uma quantidade muito grande para utilizar como vetor de
características. Portanto, como visto em \textcite{kaltwang2012continuous}, são
utilizados somente os primeiros 500 coeficientes, que são utilizados no formato
zigzag. Por exemplo, dada uma matriz $A$, sua transformação para o formato
zigzag, $A_{zz}$, pode ser vista na \equacao{eq:zigzag}.

\begin{equation}
  A =
  \begin{bmatrix} 1 & 2 & 3 \\ 4 & 5 & 6 \\ 7 & 8 & 9 \end{bmatrix}
  \rightarrow A_{zz} = \left[ 1, 2, 4, 7, 5, 3, 6, 8, 9 \right]
  \label{eq:zigzag}
\end{equation}

Enfim, é feita a extração do \glsxtrshort{lbp} da imagem, que, assim como para
a \glsxtrshort{dct}, deve estar em preto e branco. O cálculo do
\glsxtrshort{lbp} em si é feito utilizando o módulo scikit image \footnotemark,
que possui diversas ferramentas para processamento de imagens. No entanto, o
\glsxtrshort{lbp} deve ser aplicado em blocos da imagem, isto é, a imagem deve
ser dividida em $N$ blocos, e então, são calculados os \glsxtrshort{lbp} de
cada bloco, que são concatenados em um único vetor de características. Para
isso, a imagem deve ser redimensionada de forma que ela seja divisível pela
quantidade de blocos. Um exemplo da divisão da imagem pode ser visto na
\figura{fig:lbp_div}.

\footnotetext{Disponível em: \url{https://scikit-image.org}.}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.55\textwidth} % Regula o tamanho da figura
        \caption{Divisão feita para cálculo do \glsxtrshort{lbp} da
        imagem.}\label{fig:lbp_div}
        \smallcaption{Legenda: Neste exemplo, a imagem é divida em $2\times2$
        blocos de $12\times12$ \textit{pixels}.}
        \includegraphics[width=\linewidth]{Imagens/imageDivLBP}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Como pode ser visto na \figura{fig:lbp_div}, existem alguns parâmetros que
devem ser definidos para dividir a imagem nestes blocos, são eles:

\begin{itemize}
  \item Altura do bloco -- $B_h$: 13 \textit{pixels}
  \item Largura do bloco -- $B_w$: 14 \textit{pixels}
  \item Quantidade de blocos -- $N$: $9\times9$
  \item Altura da imagem -- $H$: $9\times13 = 117$ \textit{pixels}
  \item Largura da imagem -- $W$: $9\times14 = 126$ \textit{pixels}
\end{itemize}

Estes parâmetros são os mesmos que foram utilizados em
\textcite{kaltwang2012continuous}. Além disso, o \glsxtrshort{lbp} também
possui dois parâmetros: o raio e número de pontos, definidos em 8 e 2,
respectivamente.

\subsubsection{Criação dos Modelos}

O treinamento das redes \glsxtrshort{mlp} é bastante simples, basta definir a
quantidade de camadas escondidas ($n_{h}$), quantidade de neurônios ($k_h$) em
cada camada e o tipo de função de ativação ($f(net)$). Na
\tabela{tab:param_mlp} é possível ver os valores escolhidos para cada modelo
utilizado no reconhecimento de dor.

\begin{table}[!htb]
  \centering
  \caption{Parâmetros dos modelos \glsxtrshort{mlp}.}\label{tab:param_mlp}
  \begin{tabular}{cclcc}
    \toprule
    Modelo & $n_h$ & $k_h$ & $\sum k_h$ & $f(net)$ \\
    \toprule
    PTS & 5 & $\left[ 100, 80, 40, 20, 5 \right]$ & 245 & \glsxtrshort{relu} \\
    DCT & 8 & $\left[ 300, 200, 100, 50, 25, 10, 10, 5 \right]$ & 700 & \glsxtrshort{relu} \\
    LBP & 11 & $\left[ 3000, 2000, 1000, 500, 250, 100, 50, 25, 15, 10, 5 \right]$ & 6955 & \glsxtrshort{relu} \\
    \bottomrule
  \end{tabular}
  \smallcaption{Fonte: Autor.}
\end{table}

Como o objetivo é que os modelos sejam capazes de calcular a intensidade da
dor, ou seja, realizar uma regressão, é necessário que a função de ativação
seja linear. Além disso, a intensidade de dor não pode ser negativa, logo,
a \glsxtrlong{relu} (\glsxtrshort{relu}) é uma boa função de ativação, pois ela
engloba todas essas características.

Já para a determinação da quantidade (total) de neurônios nas camadas
escondidas, foi utilizada a heurística de Hecht-Nielsen
\cite{hecht1987kolmogorov}, que determina que uma função de $n$ entradas e $m$
saídas pode ser descrita por uma rede com $n$ neurônios na camada de entrada,
$2n+1$ neurônios na camada escondida e $m$ neurônios na camada de saída. A
fórmula matemática está descrita na \equacao{eq:hecht}.

\begin{equation}
  k_h \leq 2n + 1
  \label{eq:hecht}
\end{equation}

Onde, $n$ é o número de entradas da rede. Dessa forma, as quantidades máximas
de neurônios para cada modelo podem ser vistas nas Equações \eqref{eq:max_pts},
\eqref{eq:max_dct} e \eqref{eq:max_lbp}.

\begin{align}
  k_{h_{PTS}} & \leq  2\times128 + 1 \therefore k_{h_{PTS}} \leq 257
  \label{eq:max_pts}\\
  k_{h_{DCT}} & \leq 2\times500 + 1 \therefore k_{h_{DCT}} \leq 1001
  \label{eq:max_dct}\\
  k_{h_{LBP}} & \leq 2\times4779 + 1 \therefore k_{h_{LBP}} \leq 9559
  \label{eq:max_lbp}
\end{align}

A partir disto, a distribuição dos neurônios foi feita a partir da tentativa e
erro. A quantidade de neurônios foi reduzida enquanto era possível fazê-lo sem
perder desempenho da rede.

Como já discutido na Seção \ref{sec:rec-dor}, a base de dados McMaster-UNBC
possui um grande desbalanceamento de dados, como pode ser visto na
\figura{fig:pain_db}, ela possui uma grande quantidade de imagens com
intensidade de dor nula. Na \figura{fig:pain_db} é possível ver o histograma da
quantidade de imagens para cada intensidade de dor, assim, é possível ver que
existem mais de $40.000$ imagens com intensidade de dor nula, nenhuma imagem
com intensidade de dor maior que $14$ e somente uma imagem com intensidade
maior que $13$.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Distribuição das intensidades de dor.}\label{fig:pain_db}
        \includegraphics[width=\linewidth]{Imagens/Database_format}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Dessa forma, para diminuir a influência dessa grande quantidade de dados nulos,
as $30.000$ primeiras amostras nulas foram removidas

Para o treinamento foi feita uma divisão de $90\%/10\%$, do total de dados
úteis, para treinamento e validação, respectivamente. O método de treinamento
foi o da descida do gradiente.


\subsection{Interface Gráfica}

A interface gráfica é importante, pois ela que é a ``cara'' do sistema.
Existem diversos detalhes que devem ser considerados na hora de projetar o
visual de um robô, especialmente se ele for possuir características humanas, e,
caso isso não seja feito com cuidado, pode causar problemas de aceitação das
pessoas que devem interagir com o robô \cite{broadbent2013robots}. Por conta
disso, optou-se por um visual mais \textit{cartoon}, longe de uma aparência
humana. O modelo da robô \glsxtrshort{nath} virtual, presente na interface
gráfica, pode ser visto na \figura{fig:nath_visu}.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.5\textwidth} % Regula o tamanho da figura
        \caption{Visual da \glsxtrshort{nath}.}\label{fig:nath_visu}
        \includegraphics[width=0.9\linewidth]{Imagens/visual_nath}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Para não ser somente uma imagem estática fazendo perguntas para os pacientes,
foram feitas algumas animações para serem usadas durante as ações da
\glsxtrshort{nath}. Ela é capaz de: piscar, acenar, andar para a
direita/esquerda e ``falar'', a fala é indicada pela movimentação da forma de
onda no corpo do robô.

Já que o projeto não pode demandar muitos recursos computacionais, pois o
objetivo é que ele seja facilmente embarcado em hardware, as animações foram
feitas utilizando \textit{sprite sheets}, que é uma técnica comum em jogos que
utilizam \textit{pixel art}. Com esta técnica, o desenvolvedor é responsável
por montar, \textit{frame} a \textit{frame}, a animação desejada; de forma que,
cada \textit{frame} fique lado a lado num único arquivo de imagem, podendo, ou
não, utilizar arquivos separados para ações diferentes. Um exemplo disso pode
ser visto na \figura{fig:acenando}, que é a \textit{sprite sheet} para a ação
de acenar.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da \glsxtrshort{nath} acenando.}\label{fig:acenando}
        \includegraphics[width=\linewidth]{Imagens/acenando}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

A vantagem desta forma de realizar as animações é que a única operação
necessária é recortar os frames da imagem total, que é um processo muito mais
simples do que fazer os desenhos \textit{online}, utilizando uma
\textit{engine} gráfica, por exemplo.

Para controlar as \textit{sprite sheets} e também outras atividades audio
visuais da \glsxtrshort{nath}, foi utilizado o módulo \textit{pygame}, que
possui ferramentas para fazer o recorte dos \textit{frames}, tocar sons
(necessário na hora da \glsxtrshort{nath} fazer perguntas), entre outras.

\subsection{Reconhecimento Facial, \textit{Text-to-Speech} e \textit{Speech-to-Text}}

No reconhecimento facial foi utilizado um modelo pré-treinado, que utiliza como
vetor de características os 64 pontos fiduciais da face. Para utilizar o modelo
basta cadastrar um par paciente/nome, que o modelo será capaz de identificar
esta pessoa através das imagens captadas pela câmera da \glsxtrshort{nath}.

Grande parte dos sistemas que possuem uma voz de boa qualidade são pagos. Por
conta disso, optou-se por utilizar a API do Google para realizar o
\textit{Text-to-Speech}. Esta API recebe um texto como entrada e retorna um
arquivo mp3 com a voz tradicional do Google, ela possui alguns problemas de
entonação, mas, para uma fase de testes é suficiente.

De forma similar ao \textit{Text-to-Speech}, o \textit{Speech-to-Text} também
foi implementado utilizando uma API do Google. Essa API tem como entrada
o áudio e retorna a transcrição mais provável.

\subsection{Abordagem de Atendimento}

A abordagem de atendimento do sistema \glsxtrshort{nath} foi pensada para ser
agradável e de fácil aceitação. Desde a parte visual até as perguntas
realizadas pelo sistema buscou-se algo que fosse mais sútil para evitar
desconforto dos pacientes ao interagir com a \glsxtrshort{nath}.

Na parte gráfica, como já dito, evitou-se o conceito de humanização devido as
dificuldades de aceitação da população ao interagir com visuais semelhantes aos
humanos \cite{broadbent2013robots}. O conceito de \textit{cartoon} trás uma
identidade mais conectada com a infância e, com isso, entrega um conforto maior
principalmente para aquelas pessoas que necessitam de atenção especial no trato.

As palavras escolhidas para as perguntas a serem realizadas durante abordagem
também foram pensadas para um diálogo mais polido, evitando palavras que
pudessem ser interpretadas como mais pesadas. Essa linguagem foi escolhida para
combinar com a identidade visual da \glsxtrshort{nath} e não gerar certo
desalinhamento entre o visual e a linguagem que pudesse, também, gerar certo
desconforto ao usuário.

Para esta etapa do projeto teve-se que optar por usar a voz padrão do Google
como voz da \glsxtrshort{nath}, no entanto, devido à dificuldade que esta voz
possui em expressar as entonações das frases, ela pode dificultar o
entendimento das perguntas feitas. Para fases mais avançadas do projeto, esse
é um ponto a ser melhorado.

\subsubsection{Interação com o paciente}

Após a abordagem dos pacientes, o protocolo de atendimento é iniciado, com o
intuito de verificar se é necessário uma reclassificação do paciente. Nesta
etapa, as perguntas presentes no \glsxtrshort{stm} são realizadas pela
\glsxtrshort{nath}, utilizando a ferramenta de \textit{Text-to-Speech}, e as
respostas do paciente são captadas pelo sistema de conversão fala-texto.
Entretanto, trabalhar com linguagem não estruturada, como é a fala humana, gera
implicações para que o sistema relacione o que é dito pelo paciente com o que é
esperado pelo \textit{script} de atendimento. Uma das soluções adotadas para
tratar este tipo de empecilho foi utilizar as técnicas de compreensão de texto
do \glsxtrshort{pln}.

As técnicas de pré-processamento de texto são utilizadas para tratar a resposta
convertida em texto, de forma a facilitar a obtenção de informações relevantes
destas frases. As técnicas utilizadas pela \glsxtrshort{nath} para tratar as
falas dos pacientes são: tokenização, remoção de \textit{stopwords} e análise
de n-gramas\footnotemark. Nestas etapas, o sistema converte a frase captada em uma lista com
todas as palavras, retira os elementos textuais semanticamente irrelevantes
para o sentido da sentença, e lista todos os pares de palavras da frase,
respectivamente.

\footnotetext{ N-gramas são uma sequência de n elementos textuais, neste contexto, palavras.}

A partir deste pré-processamento nas sentenças, o sistema verifica a existência
de um conjunto de palavras escolhidas especificamente para cada pergunta,  e se
nenhum destes termos é precedido de uma negativa, o que mudaria o seu sentido.
Para analisar este tipo de negativa na frase, a lista de
bigramas\footnotemark é utilizada. A \figura{fig:exemplo_pre_processamento}
representa uma das perguntas do protocolo de atendimento, duas possíveis
respostas, e como o pré-processamento descrito é aplicado.

\footnotetext{ Bigramas são n-gramas cujo n=2.}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.7\textwidth} % Regula o tamanho da figura
        \caption{Exemplo de pré-processamento textual.}\label{fig:exemplo_pre_processamento}
        \includegraphics[width=\linewidth]{Imagens/exemplo_pre_processamento}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Para a resposta 1, o retorno do sistema é uma confirmação da pergunta
realizada, já que um dos termos de interesse foi encontrado no texto, e o seu
bigrama não apresenta uma negativa. Para a resposta 2, apesar da presença do
termo de interesse ``irradia'', o fato de existir uma negativa precedendo-o no
bigrama faz com que o retorno para esta frase seja negativo.

Além das técnicas de análise textual citadas anteriormente, o sistema presume
que as respostas dos pacientes serão moderadamente simples, já que não é
utilizado um sistema de análise de sentimento ou um dicionário de sinônimos
para um estudo mais complexo das respostas. Portanto, no início da abordagem, é
pedido para que o paciente mantenha suas respostas em termos simples, quando
possível em ``sim'' ou ``não''. Não sendo este o caso, as técnicas descritas
anteriormente são utilizadas.

\subsection{Localização}

Para o sistema de localização, a implementação inicial foi baseada em detectar
os pacientes na sala, e então, através da matriz de transformação de
perspectiva ($T$), extrair as coordenadas 2D do paciente no plano da sala de
espera. O efeito final dessa transformação pode ser visto na
\figura{fig:transf_pers}, é como se a câmera fosse transportada de forma que
ela ficasse paralela ao plano do chão.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.5\textwidth} % Regula o tamanho da figura
        \caption{Transformação de perspectiva.}\label{fig:transf_pers}
        \includegraphics[width=\linewidth]{Imagens/transfPerspectiva}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

A matriz de transformação $T$ é calculada utilizando a biblioteca
OpenCV, para isso, é necessário estabelecer um plano de referência
($\alpha$), a partir do qual, será calculada a transformação de perspectiva.

Apesar de tudo, essa implementação apresentou diversos problemas nos testes
iniciais, o mais crítico foi a incapacidade de separar as posições das pessoas
caso elas estivessem próximas e os ruídos causados por conta disso. Este
problema ocorre devido à sobreposição da detecção das pessoas, e, combinada à
uma baixa altura da câmera, estes dois efeitos fazem com que o retângulo de
detecção das pessoas se sobreponham, assim, ao realizar a transformação, as
posições resultantes são muito próximas, embora na realidade exista uma
distância considerável. Esta forma de implementação só funcionaria de forma
satisfatória em salas com um grande pé direito, maior que $3.5\,m$ pelo menos,
já que os testes foram feitos com aproximadamente $2.5\,m$.

Portanto, para contornar este problema optou-se por simular o sistema
localização, ou seja, as posições dos pacientes são definidas num arquivo JSON
(\textit{JavaScript Object Notation}), e o sistema de localização lê este
arquivo como se estivesse lendo e transformando as posições das pessoas que
estão na câmera.  No exemplo da \figura{fig:sim_loc} é possível ver como o
sistema funciona, existem três pacientes não identificados na sala,
representados pelos pontos brancos, e a \glsxtrshort{nath}, representada pelo
ponto rosa, assim que um paciente é identificado pela \glsxtrshort{nath} ele
recebe uma cor única para representá-lo na visualização do sistema de
localização.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.6\textwidth} % Regula o tamanho da figura
        \caption{Simulador do sistema de localização.}\label{fig:sim_loc}
        \smallcaption{Legenda: A origem do sistema de coordenadas é o canto
        esquerdo superior, o sentido $+x$ é para a direita e o $+y$ é para baixo.}
        \includegraphics[width=\linewidth]{Imagens/simLocalizacao}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Como pode ser visto na \figura{fig:sim_loc}, também foi implementada a
simulação da movimentação da \glsxtrshort{nath}, isso foi importante para
determinar qual paciente ela havia identificado ou atendido, e então fazer as
atualizações necessárias.
