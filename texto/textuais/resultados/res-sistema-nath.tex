\section{Sistema NATH}

O sistema da \glsxtrshort{nath} foi implementado e funciona de acordo com o
esperado, cadastrando novos pacientes que entram na sala de espera, monitorando
a intensidade de dor dos pacientes presentes, e, com o aumento da intensidade
de dor, o sistema faz que com que o robô se aproxime do paciente e inicia o
atendimento para uma possível reclassificação. Nesta seção, serão apresentados
os resultados do sistema de reconhecimento de dor, onde, foi necessária a
utilização direta dos conceitos vistos na Seção \ref{sec:rec-dor}, da interface
de interação com o paciente e do sistema de \glsxtrshort{pln}.

\subsection{Reconhecimento de Dor}

Nesta seção serão apresentados os resultados obtidos com os modelos de
reconhecimento de dor. Nas Figuras \ref{fig:pain_pts}, \ref{fig:pain_dct} e
\ref{fig:pain_lbp} é possível ver o resultado de cada modelo de forma
individual, as figuras mostram o valor real de dor e o valor calculado pelos
modelos.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Cálculo da intensidade de dor utilizando o modelo PTS.}\label{fig:pain_pts}
        \includegraphics[width=\linewidth]{Imagens/Result_PTS_MSE_0_8201058765524477}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Cálculo da intensidade de dor utilizando o modelo DCT.}\label{fig:pain_dct}
        \includegraphics[width=\linewidth]{Imagens/Result_DCT_MSE_0_14722633545150193}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Cálculo da intensidade de dor utilizando o modelo LBP.}\label{fig:pain_lbp}
        \includegraphics[width=\linewidth]{Imagens/Result_LBP_MSE_0_12270532525242732}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Analisando graficamente o desempenho dos modelos é possível confirmar que, de
fato, as redes \glsxtrshort{mlp} foram capazes de aprender a realizar a
regressão da intensidade de dor. Além disso, na \figura{fig:pain_mean} é
possível ver o resultado da média dos modelos anteriores, nela é possível notar
uma pequena redução do ruído da saída em relação ao uso independente dos
modelos.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Cálculo da intensidade de dor utilizando a média dos modelos.}\label{fig:pain_mean}
        \includegraphics[width=\linewidth]{Imagens/Result_ALL_MSE_0_17919408440421966}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Também foi feito o cálculo do \glsxtrshort{mse} e do coeficiente de correlação
para cada um dos modelos e também para a média, o resultado pode ser visto na
\tabela{tab:mse_mod}.

\begin{table}[!htb]
  \centering
  \caption{\glsxtrshort{mse} dos modelos \glsxtrshort{mlp}
  obtidos.}\label{tab:mse_mod}
  \begin{tabular}{ccccc}
    \toprule
    {} & PTS & \glsxtrshort{dct} & \glsxtrshort{lbp} & $(\text{PTS}+\text{DCT}+\text{LBP})/3$ \\
    \toprule
    \glsxtrshort{mse} & 0.820 & 0.147 & 0.122 & 0.179 \\
    CORR              & 0.880 & 0.979 & 0.983 & 0.977 \\
    \bottomrule
  \end{tabular}
  \smallcaption{Fonte: Autor.}
\end{table}

Como visto na \tabela{tab:mse_mod}, além de um baixo erro, os modelos obtidos
apresentam um alto grau de correlação com os dados reais de intensidade de dor.
Levando em conta que o trabalho visto em \textcite{kaltwang2012continuous}
apresenta um \glsxtrshort{mse} de $1.373$ para a média dos modelos, e uma
correlação de $0.547$, também para a média; os resultados obtidos com esta
implementação são satisfatórios. Vale a pena lembrar que, em
\textcite{kaltwang2012continuous} é feita a validação do tipo \textit{leave one
subject out}, que, acaba sendo uma validação mais exigente do que a validação
feita neste projeto (\textit{test/train split} de $10/90\%$).

Na \figura{fig:pain_example} é possível ver um exemplo utilizando o modelo de
reconhecimento de dor. Neste exemplo, a escala de dor é dividida em cinco
intervalos iguais de aproximadamente $3$ unidades de dor (na escala
\glsxtrshort{pspi}), para cada intervalo é associada uma imagem para
representar o nível de dor.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Exemplo de uso do modelo de reconhecimento de dor.}\label{fig:pain_example}
        \includegraphics[height=0.16\linewidth]{Imagens/1_no_pain}
        \includegraphics[height=0.16\linewidth]{Imagens/2_mid_pain}
        \includegraphics[height=0.16\linewidth]{Imagens/3_high_pain}\\
        \includegraphics[height=0.16\linewidth]{Imagens/4_higher_pain}
        \includegraphics[height=0.16\linewidth]{Imagens/5_max_pain}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Os resultados obtidos são extremamente satisfatórios, dado o baixo
\glsxtrshort{mse} dos modelos, todavia, existem alguns pontos que precisam de
uma atenção especial, por exemplo, embora todo o pré-processamento feito para
minimizar as variações devido à mudanças na pose do paciente (distância da
câmera, rotação do rosto e etc), os modelos ainda apresentam variação na
intensidade de dor dependendo da distância que o paciente está da câmera, ou
então da intensidade de luz do local. Para que estes problemas sejam
resolvidos, é necessário um estudo muito mais profundo, além de também utilizar
técnicas mais sofisticadas, tanto de processamento de imagens, quanto de
inteligência artificial. Embora não tenham sido utilizados neste trabalho, os
modelos baseados em \glsxtrlong{dl} possuem uma capacidade muito maior para
solucionar estes problemas.

\subsection{Reconhecimento Facial}

Durante os testes realizados este sistema se mostrou bastante robusto em
relação à mudanças na orientação da face da pessoa e luminosidade. No entanto,
como ele é baseado nas características geométricas da face, expressões com uma
alta intensidade de dor podem fazer com que essas características fiquem
distorcidas, fazendo com que o modelo não seja capaz de identificar a pessoa.
Dessa forma, para uma futura evolução do projeto, este é um sistema que
necessita de mais desenvolvimento, de forma a melhorar a robustez à este tipo
de variação.

\subsection{Interface do Usuário}

A interface gráfica final pode ser vista na \figura{fig:nath_iface}, nela são
exibidas as ações da \glsxtrshort{nath}. Além disso, também são emitidos alguns sinais
sonoros para auxiliar a interação com o sistema, por exemplo, quando uma
pergunta é feita ao paciente é emitido um \textit{beep} que indica que o
paciente pode começar a responder; e, quando é necessário tirar uma foto do
paciente para cadastro no sistema de reconhecimento facial, é emitido um som de
uma câmera fotográfica, indicando que a foto foi tirada.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{Interface gráfica da \glsxtrshort{nath}.}\label{fig:nath_iface}
        \includegraphics[width=\linewidth]{Imagens/nath_iface}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

As informações sobre o nível de dor do paciente observado não são mostradas na
interface para evitar práticas maliciosas com o objetivo de acelerar o
atendimento.

As \textit{sprite sheets} da \glsxtrshort{nath}, criadas para melhorar a
interação com o robô virtual, podem ser vistas nas Figuras \ref{fig:acenando},
\ref{fig:and_dir}, \ref{fig:and_esq}, \ref{fig:fal_pisc}, \ref{fig:fal} e
\ref{fig:pisc}.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da ação de andar para a direita.}\label{fig:and_dir}
        \includegraphics[width=\linewidth]{Imagens/andando_direita}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da ação de andar para a esquerda.}\label{fig:and_esq}
        \includegraphics[width=\linewidth]{Imagens/andando_esquerda}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da ação de falar e piscar.}\label{fig:fal_pisc}
        \includegraphics[width=\linewidth]{Imagens/falando_piscando}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da ação de falar.}\label{fig:fal}
        \includegraphics[width=\linewidth]{Imagens/falando}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

\begin{figure}[!htb]
      \centering
      \begin{minipage}{\textwidth} % Regula o tamanho da figura
        \caption{\textit{Sprite sheet} da ação de piscar.}\label{fig:pisc}
        \includegraphics[width=\linewidth]{Imagens/piscando}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}
