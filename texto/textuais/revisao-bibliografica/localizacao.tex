\section{Localização dos Pacientes}\label{sec:localizacao}

Denomina-se localização ao estabelecimento geográfico de um eixo de coordenadas para
localizar determinados elementos, sendo estes elementos os pacientes na sala de espera e o robô móvel.
Neste capítulo serão abordados alguns estudos e análises dos métodos utilizados para o rastreamento
de objetos e/ou pessoas. Por fim, serão descritos os principais componentes do sistema de localização
dos pacientes e do robô na sala de espera, que utiliza como dados de entrada as imagens do sistema
de câmeras fixado na sala de espera.

Como parte do resultado do avanço tecnológico houve o aumento do poder de processamento
dos computadores, a disponibilidade de câmeras de alta qualidade a custos cada vez
menores e a diminuição dos custos de armazenamento de dados. Por conta disso, houve um aumento no interesse
em análise automatizada de vídeos por meio de algoritmos, tanto na área de
pesquisas acadêmicas, quanto no desenvolvimento de produtos comerciais. Dessa forma, o
monitoramento realizado por vídeo têm se tornado um meio cada vez mais acessível e eficaz
para espaços públicos e privados.

\textcite{yilmaz2006object} citam algumas práticas que utilizam análise
automatizada de vídeo que têm gerado o interesse da comunidade de pesquisa, das
citadas por ele, algumas são utilizadas no presente projeto, sendo elas:

\begin{enumerate}[label=$\bullet$]
    \item Controle na navegação de veículos automatizados;
    \item Interação homem-máquina;
    \item Reconhecimento baseado em movimentos;
    \item Vigilância automatizada.
\end{enumerate}

De acordo com \textcite{yilmaz2007object}, há três passos importantes na
análise automatizada de uma imagem, que são: a detecção dos objetos a serem
acompanhados, o rastreamento desses objetos de quadro para quadro e a análise
do objeto rastreado para o conhecimento da sua ação, e a dificuldade
relacionada a essa sequência de passos está associada a fatores como ruídos,
variações de luz no ambiente, complexidade do movimento do objeto, não rigidez
ou natureza articulada do objeto, oclusões parciais ou totais e a necessidade
de processamento em tempo real.

\subsection{Detecção dos Pacientes}

Segundo \textcite{yilmaz2006object}, é possível realizar a detecção de objetos
por meio da utilização da informação temporal, sendo obtida através da
diferença entre quadros consecutivos. Ao iniciar o sistema, um modelo é criado
para representar o fundo da cena, que não deve possuir objetos de interesse ou
algum elemento em movimento, este modelo é denominado \textit{background}, que
servirá como base de comparação com as futuras imagens do sistema, onde
qualquer variação significativa nos quadros obtidos posteriormente
corresponderá a um objeto em movimentação, denominado \textit{foreground}.

Na obtenção da imagem resultante, pensando no contexto do presente projeto,
alguns fatores podem gerar falsos positivos, dificultando a detecção correta do
\textit{foreground}, são eles: ruídos, sombras e variações de iluminação.

\subsubsection{Ruídos}

Como em qualquer tipo de sinal, os ruídos são a parte indesejada das imagens,
segundo \textcite{sampat2005handbook} existem basicamente seis tipos de ruído
que podem afetar imagens, são eles:

\begin{enumerate}[label=$\bullet$]
    \item Ruído de quantização;
    \item Ruído do tipo ``sal e pimenta'';
    \item Ruído \textit{Heavy-tailed};
    \item Ruído relativo à contagem de fótons;
    \item Ruído relativo à granulação em fotografias;
    \item Ruído térmico.
\end{enumerate}

Métodos mais robustos, como o proposto por \textcite{stauffer2000learning}, que
modelam movimentos repetitivos através de múltiplas gaussianas, ou o proposto
por \textcite{haritaoglu2000w}, que utiliza mapas de alterações para adaptar o
modelo, obtém resultados melhores no tratamento de ruídos quando comparados com
métodos menos sofisticados, como o que pode ser visto em
\textcite{seo1997ball}.

\subsubsection{Sombras}

Com a projeção das sombras no fundo da cena, os valores dos \textit{pixels} dessa
superfície são alterados, podendo ser considerados diferentes do modelo do
\textit{background}, sendo então, identificados como parte do
\textit{foreground}. Devido à sombra ser conexa com o objeto que o projeta, o
sistema define ambos como parte do mesmo objeto, gerando problemas, pois quando
a sombra de dois ou mais objetos se interceptam pode ocorrer uma falsa
adjacência entre os objetos, fazendo com que sejam agrupados e considerados
como um único corpo, portanto, dificultando a detecção.

\subsubsection{Variações de Iluminação}

Variações na iluminação do ambiente alteram os valores dos \textit{pixels}, fazendo com
que difiram do modelo e sejam identificados como \textit{foreground}, gerando
ruídos ou podendo até, em casos extremos, identificar uma imagem toda como
\textit{foreground}. Embora a proposta do local de aplicação do projeto seja um
ambiente que não sofra influência externa, fica claro a importância da
estabilidade da iluminação do local.

\subsubsection{Representação}

Após a realização de todos os tratamentos, a imagem resultante possuirá apenas
o objeto de interesse como informação, como pode ser visto na \figura{fig:sistDeteccao}.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{\textwidth} % Regula o tamanho da figura
                \caption{Fluxo Sequencial da execução do Sistema de
                Detecção}\label{fig:sistDeteccao}
                \includegraphics[width=\linewidth]{Imagens/FluxoDeteccao.png}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}

\subsection{Sistema de Rastreamento}

De modo simplificado, rastreamento pode ser entendido como a identificação do
mesmo objeto de desejo em quadros sucessivos, sendo normalmente empregado, em
aplicações que necessitam da localização do objeto em toda sequência de quadros
de um vídeo. Já a trajetória do objeto ao longo do tempo é utilizada com maior
frequência na detecção e reconhecimento de eventos, que será abordada de forma
mais detalhada na \ref{sec:reconhecimento-comportamento}.

A seguir são apresentados alguns dos sistemas de rastreamento de objetos e/ou
pessoas presentes na literatura que são mais relevantes para este projeto.
Apesar de alguns sistemas de rastreamento encontrados na literatura
desconsiderarem alguns fatores que geram dificuldades, e, possuírem algumas
limitações relacionadas às condições do ambiente, sistemas robustos conseguem
lidar com tais fatores através da utilização de técnicas específicas, obtendo
resultados promissores quando aplicados em situações reais.

\textcite{haritaoglu2000w} desenvolveram um sistema de segurança que realiza o
monitoramento das atividades de múltiplas pessoas em ambientes externos em
tempo real, sendo realizado através de imagens monocromáticas capturadas por
uma única câmera fixa, denominado de W4. Nesse sistema, as regiões na imagem
que correspondem às pessoas são detectadas através de uma técnica de subtração
de fundo proposta pelo próprio autor, sendo a etapa de associação realizada
através de parâmetros retirados da posição do centroide, silhueta da região
detectada e \textit{bounding box}. Durante o rastreamento, caso ocorra a
oclusão entre duas ou mais pessoas, o sistema detecta a posição da cabeça das
pessoas através do sistema denominado \textit{Hydra}
(\textcite{haritaoglu1999hydra}).  A posição de cada pessoa envolvida na
oclusão parcial é estimada através da posição das cabeças e as trajetórias
dessas pessoas são determinadas individualmente.  O sistema W4 é capaz de
manter armazenadas as informações relacionadas à textura e forma da região
correspondente à pessoa, com isso, casos de desaparecimento da pessoa de
interesse, causado por uma oclusão total ou pela saída do ambiente monitorado,
possuem tratamento. Quando essa pessoa é detectada novamente na cena, ela é
identificada através dessas informações e seu rastreamento é retomado.

\textcite{lu2001color} apresentaram um sistema de rastreamento que se baseia na
informação relacionada às cores das pessoas seguidas. Nesse sistema, a detecção
acontece através de uma técnica de subtração de fundo baseada em
\textcite{wren1997pfinder}, onde a cor de cada \textit{pixel} na imagem é modelada por
uma distribuição gaussiana. Logo após, um método de detecção e remoção de
sombras é aplicado para evitar que as sombras das pessoas sejam identificadas
como \textit{foreground}. Ao verificar que uma nova pessoa entrou na cena,
inicia-se seu rastreamento através da análise da variação do centroide,
calculando o histograma de cor dos \textit{pixels} da região correspondente à pessoa
para utilizações futuras em casos de oclusões. Quando duas ou mais pessoas se
ocluem, o sistema passa a considerar um novo corpo, com isso, as trajetórias
dessas pessoas são determinadas conforme o deslocamento da região resultante da
união. Quando a oclusão termina e a separação acontece, o rastreamento
individual é retomado.

\textcite{zhao2004tracking} desenvolveram um sistema que realiza o rastreamento
de múltiplas pessoas através de câmeras. O método utilizado para a subtração de
fundo também foi o proposto por \textcite{wren1997pfinder}, assumindo que todas
as sombras possuem a mesma direção, resultantes de uma única fonte de luz, o
sistema identifica e remove as regiões que correspondem às sombras das pessoas.
A detecção de pessoas na imagem é um processo recursivo: primeiramente é
realizada a detecção das posições de todas as cabeças no \textit{foreground},
em seguida é posicionada uma elipse, que representa o modelo de uma pessoa,
abaixo de cada cabeça detectada e só então essa pessoa é removida do
\textit{foreground}. Esse processo é repetido até que nenhuma cabeça seja
detectada, sendo o critério de associação aquela que minimiza a transformação
de uma elipse para seu equivalente no quadro seguinte. Esse sistema não realiza
o tratamento de oclusões totais, porém, lida de forma adequada com oclusões
parciais desde que as cabeças das pessoas oclusas possam ser detectadas.

\textcite{lei2006real} apresentaram um sistema de rastreamento de objetos e
classificação de estados, onde foi utilizado o método \glsxtrshort{mog}
\cite{stauffer1999adaptive} para subtração de fundo, e se basearam no método
desenvolvido por \textcite{horprasert1999statistical} para a eliminação das
sombras e ruídos. As trajetórias são determinadas através de métricas de
associação relacionadas à posição do centroide, área, cor e ao tamanho dos
eixos da elipse contida na região detectada na imagem. Em casos de oclusões, as
trajetórias são determinadas através do deslocamento da região da soma dos
objetos. Para a realização do gerenciamento do rastreamento de múltiplos
objetos é proposto um sistema de classificação de estados, que classifica a
situação dos objetos em categorias preestabelecidas, como: aparecido, maduro,
ocluso, temporariamente indisponível, desaparecido, reaparecido e fora de cena.
A partir dessas classificações é possível realizar o tratamento de oclusões
parciais e de oclusões totais.

Na \tabela{tab:relSistRastreamento} é possível observar de modo resumido todos
os sistemas que foram citados acima e os métodos utilizados em cada um deles.

\begin{table}[!htb]
    \centering
      \caption{Sistemas de rastreamento}\label{tab:relSistRastreamento}
      \scalebox{0.8}{
          \begin{tabular}{m{0.2\textwidth}m{0.2\textwidth}m{0.2\textwidth}m{0.2\textwidth}m{0.3\textwidth}}
            \toprule

            \textbf{Autor} & \textbf{Detecção do \textit{Foreground}} &
            \textbf{Sombras} & \textbf{Associação} & \textbf{Tratamento de
            Oclusão} \\ \toprule

            \textcite{haritaoglu2000w} & Subtração de fundo proposta pelo autor
            & Não trata & Centroide, \textit{bounding box} e contorno &
            Detecção de cabeças por Hydra \cite{haritaoglu1999hydra}. \\
            \midrule

            \textcite{lu2001color} & \textcite{wren1997pfinder} & Método do
            próprio autor. & Centroide. & Rastreamento do centroide do grupo.
            Comparação de cor para identificar os alvos após oclusões. \\
            \midrule

            \textcite{zhao2004tracking} & \textcite{wren1997pfinder} & Método
            do próprio autor & Modelo em forma de elipse & Detecção de cabeças
            \\ \midrule

            \textcite{lei2006real} & \textcite{stauffer1999adaptive} &
            \textcite{horprasert1999statistical} & Centroide, área, cor e
            modelo em forma de elipse. & Rastreamento do centroide do grupo.
            Comparação de cor para identificar os alvos após oclusões.   \\
            \bottomrule
          \end{tabular}
        }
        \smallcaption{Fonte: Autor.}
\end{table}


Na \figura{fig:sistRastreamento} é possível ver o processo completo do sistema de 
rastreamento após a inclusão da etapa de associação e rastreamento.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{\textwidth} % Regula o tamanho da figura
                \caption{Fluxo Sequencial da execução do Sistema de
                Rastreamento}\label{fig:sistRastreamento}
                \includegraphics[width=\linewidth]{Imagens/fluxo_rastreamento.png}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}

\subsection{Determinação da Localização}

Após a realização das etapas de identificação e rastreamento de pessoas e/ou
objetos, o sistema de localização deve definir a posição $(x, y)$ de cada
paciente, este processo utiliza duas câmeras fixas que, de
forma simultânea, capturam a mesma imagem, porém, com perspectivas diferentes.
Assim, formando o conceito de visão computacional estéreo, onde a precisão da
medição é diretamente proporcional à resolução da câmera e a qualidade da
calibração realizada.

A visão computacional estéreo é uma tentativa de reprodução do modo como os
seres humanos veem o mundo, onde as imagens separadas de dois olhos são
combinadas gerando uma única imagem tridimensional no cérebro, portanto,
pode-se dizer que a visão computacional estéreo é o ramo da visão computacional
que, a partir de um par de imagens capturadas simultaneamente e em diferentes
perspectivas, estuda o problema da reconstrução da informação tridimensional de
objetos, sendo este par de imagens definido como par estéreo. Esta técnica
baseia-se em estabelecer relação entre pontos de duas ou mais imagens
capturadas, e a partir desta correlação determina a disparidade, obtendo-se a
geometria da cena, determinando a profundidade dos objetos. Exemplos dessa
técnica podem ser encontrados em \textcite{stauffer1999adaptive}, neste
trabalho são realizadas estimações de distâncias através da medição dos
tamanhos relativos de marcas guia em imagens capturadas de uma câmera.

\textcite{pimentel2000algorithm} propuseram um sistema de visão para traçar
rotas para navegação de um veículo por meio da extração de informações do
ambiente. Os métodos utilizados foram: transformação de \textit{Hough} e
interpolação por mínimos quadrados. Já \textcite{freitas1999sistema}, propôs um
sistema de visão em conjunto com sensores de ultra som, que é utilizado para
reconhecer obstáculos que estão a uma determinada distância do veículo. No
processo de formação da imagem, a câmera projeta um ponto no espaço em um plano
imaginário, chamado de plano de imagem da câmera. Esse processo é descrito por
uma matriz denominada matriz de projeção, que somente é determinada de maneira
correta através do processo de calibração das câmeras.

\subsubsection{Calibração}

Segundo \textcite{faugeras1992camera}, a calibração de câmeras é uma etapa
fundamental em toda aplicação de visão computacional e tem como objetivo a
obtenção dos parâmetros (extrínsecos e intrínsecos) que possibilitarão mapear
coordenadas tridimensionais do mundo em coordenadas bidimensionais das imagens.

\begin{enumerate}[label=$\bullet$]
    \item \textbf{ Parâmetros Extrínsecos}: ou de orientação exterior, são
      aqueles que relacionam o sistema de coordenadas da câmera com o sistema
      de coordenadas do mundo, descrevendo a posição e orientação da câmera no
      mundo, ou seja, a relação espacial entre a câmera e o mundo.

    \item \textbf{ Parâmetros Intrínsecos}: são aqueles que definem a geometria
      interna da câmera e suas características ópticas, como distância focal da
      câmera, ponto central, distorção das lentes e fator de escala.
\end{enumerate}

Os métodos existentes na literatura basicamente se dividem em duas categorias,
calibração convencional e calibração automática de câmeras, que serão
explicadas em detalhes a seguir.

\subsubsection{Calibração Convencional}

No processo de calibração das câmeras, tanto as coordenadas de um conjunto de
pontos, quanto as coordenadas das projeções desses pontos, devem ser
conhecidas. Essas informações podem ser obtidas através do uso de um gabarito,
que normalmente é composto por um ou dois planos ortogonais com padrões
contrastantes nas suas faces. Deste modo, pode-se considerar um dos cantos do
gabarito como origem do sistema de coordenadas, tornando as coordenadas dos
padrões do gabarito e suas projeções no plano da imagem, conhecidas
\cite{tsai1986efficient}.

Apesar de serem métodos bastante exatos, segundo \textcite{faugeras1992camera},
estes métodos possuem duas limitações importantes, sendo a primeira destacada
por nem sempre ter disponível um objeto que sirva como gabarito, tornando
impossível a realização da calibração. Segundo, caso ocorra alguma alteração
dos parâmetros da câmera o gabarito deve ser posicionado novamente para a
realização de uma nova calibração.

\subsubsection{Calibração Automática}

Diferentemente da calibração convencional, na calibração automática não é
necessário o uso de objetos conhecidos previamente como gabarito. Este tipo de
calibração é realizado através da captura de diferentes imagens, de uma cena
estática, sem alteração dos parâmetros intrínsecos da câmera. Em seguida a
correspondência entre pontos presentes na imagem é realizada. Apesar de ser um
método menos preciso e que demande um maior processamento quando comparado com
os métodos convencionais, este método pode se tornar interessante quando não é
possível utilizar gabaritos para a calibração.

\subsection{Geometria Epipolar}

Através da utilização de duas câmeras que capturam a mesma cena, cada uma com o
seu centro de projeção, sendo estes não coincidentes. Cada par de imagem
capturado por essas câmeras representam duas perspectivas diferentes de uma
mesma cena estática.  A geometria epipolar estabelece uma relação geométrica
entre duas vistas capturadas nessas condições, com o objetivo de encontrar os
pontos em comum de ambas vistas.

As localizações das câmeras possuem uma grande influência na precisão da
reconstrução dos pontos em três dimensões, pois cada câmera constrói uma reta
entre sua posição e a posição do objeto de interesse, com isso, gerando
possíveis erros na projeção da posição do objeto. Para mitigar possíveis erros
ocasionados pelo posicionamento das câmeras, deve-se posicionar de modo que os
eixos centrais das câmeras formem ângulos próximos de noventa graus.

\subsection{Extração das Informações}

Neste projeto é utilizada uma combinação de técnicas já existentes
na literatura, de modo a atingir os melhores resultados possíveis, obedecendo ao
fluxo descrito na \figura{fig:sistema_localizacao}.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.75\textwidth} % Regula o tamanho da figura
                \caption{Fluxo do Sistema de Estimativa de
                Posição}\label{fig:sistema_localizacao}
                \includegraphics[width=\linewidth]{Imagens/simplif_sist_vis.png}
                \smallcaption{Fonte: Autor.}
          \end{minipage}
\end{figure}
