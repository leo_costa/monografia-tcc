\section{Processamento de Linguagem Natural}\label{sec:IntroPNL}

O \glsxtrlong{pln} (\glsxtrshort{pln}) é o conjunto de métodos para tornar a
linguagem humana acessível aos computadores
\cite{eisenstein2019introduction}. Segundo \textcite{covington1997prolog}, as
pesquisas em \glsxtrshort{pln} estão voltadas, basicamente, a três aspectos da
comunicação humana:

\begin{enumerate}[label=$\bullet$]
    \item \textbf{Som}: fonologia;
    \item \textbf{Estrutura}: morfologia e sintaxe;
    \item \textbf{Significado}: semântica e pragmática.
\end{enumerate}

De acordo com \textcite{schwindt2017manual}, a fonologia estuda a forma como os
sons funcionam na língua, em sua organização em constituintes como sílabas,
morfemas e palavras.

A morfologia é o estudo da estrutura, da formação e classificação das palavras
dentro do idioma. A sintaxe é a parte da gramática que estuda princípios e
processos pelos quais as sentenças são construídas.

Semântica é o estudo do significado e a interpretação do significado de
palavras, frases e sentenças em certo contexto. Enquanto, a pragmática estuda a
relação entre o interlocutor, o contexto e o significado das palavras.

Para representar o significado de uma sentença, é necessário que ela seja
transformada em sua forma lógica \cite{allen1995natural}. A forma lógica
identifica o relacionamento semântico das palavras com as frases, a partir de
possíveis sentidos para cada palavra. Ao obter esse relacionamento semântico, é
possível desconsiderar alguns sentidos destas palavras, já que eles se tornam
inviáveis.

Para transformar uma sentença em sua forma lógica, é necessário que ela passe
por dois tipos de processamento: morfossintático e semântico, como pode ser
visto na \figura{fig:transfSentenca}.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.85\textwidth} % Regula o tamanho da figura
            \caption{Transformações da sentença na estrutura sintática e
            forma lógica}\label{fig:transfSentenca}
            \includegraphics[width=\linewidth]{Imagens/transformacao_sentenca.png}
            \smallcaption{Fonte: Adaptado de
            \textcite{gonzalez2003recuperaccao}.}
      \end{minipage}
\end{figure}

O processamento morfossintático gera a estrutura sintática da sentença, para
isso, é necessária uma gramática, de onde serão extraídas as leis gramaticais
que irão reger a representação desta estrutura. Para obter as categorias
morfológicas das palavras, é utilizado um léxico.

O léxico também exerce papel importante no processamento semântico, onde com
informações sobre o significado dos itens lexicais, realiza-se o mapeamento da
estrutura sintática da sentença em sua forma lógica.

\subsection{Gramática}

Como visto anteriormente, a gramática tem papel indispensável no que diz
respeito à transformar uma sentença em sua forma lógica. A gramática é formada
por uma série de regras de boa formação das palavras e sentenças de um idioma.
Esse conjunto de regras permite que a gramática tenha duas funções: normativa,
onde as regras de combinação das palavras são definidas; e representativa, onde
um certo número de frases é associado a suas representações sintáticas
\cite{bouillon1998traitement}.

De acordo com \textcite{allen1995natural}, uma boa gramática deve ser:

\begin{enumerate}[label=$\bullet$]
    \item \textbf{Genérica} a ponto de aceitar o maior número possível de
      sentenças válidas;
    \item \textbf{Seletiva} de forma que reconheça casos identificados como
      problemáticos;
    \item \textbf{Seletiva} ao ponto que favoreça a compreensão de suas regras.
\end{enumerate}

% É possível representar uma gramática a partir de diversos formalismos de
% representação computacional \cite{nunes1999introduccao}. O formalismo da
% gramática de constituintes imediatos (\glsxtrlong{psg}--\glsxtrshort{psg}) é um
% destes, que é definido por uma quádrupla <T, N, P, S>, onde:
%
% \begin{enumerate}[label=$\bullet$]
%     \item T é o conjunto das palavras da língua;
%     \item N é o conjunto das categorias lexicais e funcionais;
%     \item P é o conjunto de regras de produção;
%     \item S é o símbolo inicial que pertence a N.
% \end{enumerate}

A \figura{fig:exdegram} apresenta um exemplo de gramática.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{0.65\textwidth} % Regula o tamanho da figura
                \caption{Exemplo de Gramática}\label{fig:exdegram}
                \includegraphics[width=\linewidth]{Imagens/exemplodegramatica.PNG}
                \smallcaption{Fonte: Adaptado de
                \textcite{barbosa2017introduccao}.}
          \end{minipage}
\end{figure}

Analisando a \figura{fig:exdegram}, não é nítido qual o melhor formalismo. De
acordo com os pesquisadores da área, modelos que atuam entre gramáticas livres
de contexto e gramáticas sensíveis ao contexto são os mais indicados
\cite{vieira2001linguistica}. Se tratando de \glsxtrshort{pln}, é de suma
importância que sejam utilizados critérios formais para a construção das regras
gramaticais.

\subsection{Léxico}

Outro recurso de grande utilidade para o \glsxtrlong{pln} é o léxico
(dicionário), que consiste no conjunto de palavras daquele idioma e sua relação
com os significados e categorias gramaticais.

Em um sistema de \glsxtrshort{pln}, o léxico é usado para identificar o
componente com informações semânticas e gramaticais dos itens lexicais. Um
agrupamento de informações lexicais, em formato estruturado e acessível é
conhecido como ``base de dados lexical''. O propósito de um léxico é fornecer
grande número de informações sobre as palavras, como etimologia, sintaxe,
morfologia, entre outras.

No contexto de \glsxtrshort{pln}, existem os léxicos legíveis e tratáveis por
máquinas \cite{wilks1996electric}. Dicionários legíveis por máquinas
(\glsxtrlong{mrd}--\glsxtrshort{mrd}) são utilizados para extração automática
de informações lexicais em larga escala, assim, melhorando a consistência e
uniformidade da informação. Além dos \glsxtrshort{mrd}s, existem os dicionários
tratáveis por máquinas (\glsxtrlong{mtd}--\glsxtrshort{mtd}), que são léxicos
que permitem a conversão de dicionários existentes em uma forma apropriada para
utilização em \glsxtrshort{pln}.

Um exemplo de dicionário que pode ser utilizado em computadores é o
\glsxtrlong{ecd} (\glsxtrshort{ecd}) \cite{evens2009relational}. O
\glsxtrshort{ecd} usa o \glsxtrlong{mtm}  (\glsxtrshort{mtm}), um modelo que
associa significados aos textos em quatro níveis linguísticos de representação:
semântico; sintático; morfológico; e fonético ou ortográfico.

Outro exemplo de dicionário utilizável em \glsxtrshort{pln} é o
\textit{WordNet} \cite{fellbaum1998wordnet}, uma base de dados lexical
organizada por significado e legível por máquinas. Esta base está dividida em
grupos de verbos, substantivos, advérbios e adjetivos.  Os itens lexicais são
apresentados através de seus diversos sentidos, suas definições e a relação que
possuem com outros itens lexicais. Para a construção dos relacionamentos
semânticos básicos, a sinonímia (relação entre sinônimos), o \textit{WordNet}
utiliza o conceito de \textit{synset} (conjunto de sinônimos). Utilizando este
conjunto de sinônimos uma hierarquia lexical é formada, pela
hiponímia\footnote{Relação entre uma palavra de sentido mais genérico, com
outra com sentido mais específico.} entre eles, por exemplo: (tordo, pisco de
peito ruivo) $\rightarrow$ (pássaro) $\rightarrow$ (animal, ser animado)
$\rightarrow$ (organismo, forma de vida, ser vivo)
\cite{gonzalez2003recuperaccao}.


\subsection{Processamento Morfossintático}

A partir do momento que se possui bases de dados lexicais e regras gramaticais,
é possível iniciar a transformação de uma sentença em sua forma lógica. Para
isso, o processamento morfossintático é iniciado.

O processamento morfossintático é formado pela análise morfológica e pela
análise sintática. Como tratado na Seção \ref{sec:IntroPNL}, a morfologia trata
da estrutura, da formação e classificação das palavras dentro do idioma, e seu
analisador (analisador léxico-morfológico) trata das estruturas e classifica as
palavras em determinadas categorias. Já a sintaxe trata dos grupos de palavras
que constituem os elementos de expressão de um idioma, e o analisador sintático
atua no nível de agrupamento das palavras, verificando a constituição das
palavras.

Pela análise sintática, também conhecida como \textit{parsing}, são avaliados
os modos de combinação de regras gramaticais, visando formular uma estrutura de
árvore que retrata a estrutura sintática da sentença que está sendo trabalhada.
A \figura{fig:arvoresint} apresenta um exemplo de árvore sintática. Caso a
sentença possa ter mais de um significado, o \textit{parser} (analisador
sintático) obterá todas as possíveis estruturas sintáticas que representam esta
frase.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.8\textwidth} % Regula o tamanho da figura
            \caption{Árvore sintática da frase ``eu vi o menino com a pipa''}\label{fig:arvoresint}
                \includegraphics[width=\linewidth]{Imagens/arvore_sintatica.PNG}
                \smallcaption{Fonte: Adaptado de
                \textcite{gonzalez2003recuperaccao}.}
          \end{minipage}
\end{figure}

O processamento sintático normalmente tem posição de destaque, e a semântica é
considerada uma interpretação da sintaxe \cite{nunes1999introduccao}.
Entretanto, também pode ter papel secundário, onde a sintaxe é uma projeção da
semântica. De qualquer forma, o processamento sintático é indispensável para
que o processamento semântico possa ser aplicado.

\subsection{Processamento Semântico}

A semântica não está apenas relacionada ao significado das palavras, mas também
delas em conjunto. E um dos grandes desafios do \glsxtrshort{pln}, é o
processamento semântico, pois abrange tanto morfologia e estrutura sintática,
quanto informações de protocolo social.

Segundo \textcite{allen1995natural}, em linguagem, o significado de uma
composição depende dos significados dos seus componentes, e isso é o princípio
da composicionalidade, ou seja, em uma sentença, o significado depende de cada
uma das palavras. Por isso, é importante analisar as relações dos itens
lexicais.

\subsubsection{Variações Linguísticas}

Para compreender o significado de uma sentença, deve-se estar ciente das
variações linguísticas. Essas variações podem ocorrer de diversas maneiras:
palavras diferentes com mesmo significado (variação lexical), por exemplo,
``sapato'' e ``calçado'' combinações diferentes que geram significados
diferentes para a sentença (variação lexical), por exemplo, ``restaurante do
povo'' e ``povo do restaurante''; flexão ou derivação de uma palavra (variação
morfológica); quando a variação é morfológica, mas a essência da frase
permanece (variação morfossintática), por exemplo, ``reservatório de água'' e
``reserva de água''; e  quando a palavra ou frase por indicar mais de uma
coisa, por exemplo, ``manga'' e ``queda da bolsa'' (variação semântica)
\cite{jacquemin1997expansion}.

Essas variações podem implicar em outro fenômeno, a ambiguidade, ou seja, uma
sentença pode ser entendida de mais de uma forma. A ambiguidade pode ocorrer
no nível sintático ou semântico.

\textcite{gonzalez2003recuperaccao} descrevem que a ambiguidade sintática
acontece quando um item lexical pertence a mais de uma classe gramatical, por
exemplo, na frase ``a bela casa'' a palavra ``casa'' pode ser um verbo ou um
substantivo.  Um exemplo de ambiguidade semântica pode ser visto no verbo
``passar'', nas frases ``passar a ferro'' e ``passar o exame'', na primeira
frase o verbo indica a ação de passar uma roupa, já na segunda, o verbo indica
ser aprovado.  Existem outras formas de ambiguidade, e todas elas precisam ser
resolvidas para que se seja eficiente na representação do conhecimento.

\subsection{Representação do Conhecimento}

Para processar a informação, é importante que se saiba representar o
conhecimento tido para aquela situação ou para o mundo. Uma parte da
representação de conhecimento está no nível semântico lexical, onde deve-se
associar às palavras sua estrutura conceitual e propriedades sintáticas.
Outra parte é a representação do conhecimento de contexto, agregando, assim,
significado aos conceitos já estabelecidos.

Representar o conhecimento não é uma tarefa simples, já que depende da forma
que se interpreta o significado. De acordo com \textcite{davis1993knowledge}, a
representação do conhecimento pode ser descrita como uma teoria que estabelece
o modo como o raciocínio é representado, o que é deduzível a partir do que
conhecemos e o que deve ser deduzido sobre o que já é conhecido.

Na área de inteligência artificial, \textcite{russell2002artificial} dizem
que o objetivo da representação de conhecimento é apresentar o conhecimento
de forma tratável pelo computador, de uma forma que o resultado seja útil
para a comunicação entre máquina e pessoa.

\subsection{Estratégias de Processamento}

Técnicas linguísticas podem ser utilizadas para tratar da ambiguidade, e
representar uma frase de maneira mais simples para que o computador trabalhe
com ela.

\subsubsection{Etiquetagem de texto}

Um dos primeiros passos é a etiquetagem do texto. Um etiquetador gramatical
(\textit{part-of-speech tagger}) é um sistema que, através de etiquetas (ou
\textit{tags}), identifica a categoria gramatical dos itens lexicais de um texto
\cite{bick1998structural}.  Já o etiquetador morfológico adiciona dados das
categorias morfológicas, como adjetivo e substantivo, e o etiquetador sintático
inclui informações das funções sintáticas das palavras, como objeto direto e
sujeito. Além disso, também existe a etiquetagem semântica, que adiciona
informações que dizem respeito ao significado, e, dessa forma, podem apontar nas
sentenças qual o papel de cada item lexical, como processo, estado e agente.

\subsubsection{Normalização}

A normalização morfológica é o processo de redução dos itens lexicais por meio
da conflação (processo que combina dois ou mais termos em um único) para uma
forma que busca representar classes conceituais. Existem dois métodos mais
conhecidos para conflação:

\begin{enumerate}[label=$\bullet$]
    \item \textbf{\textit{Stemming}}: Elimina os afixos vindos de derivação ou
      flexão, obtendo o denominado \textit{stem}, por exemplo, as palavras
      ``construções'' e ``construirão'' são reduzidas ao \textit{stem}
      ``constru'';

    \item \textbf{\textit{Lemmatization} (ou redução a forma canônica)}: Reduz
      os verbos ao infinitivo e os adjetivos e substantivos para a forma
      masculina no singular \cite{arampatzis2000linguistically}, por exemplo,
      as palavras ``construções''  e ``construirão''  são reduzidas para
      ``construção''  e ``construir'', respectivamente.
\end{enumerate}

\subsubsection{Eliminação de \textit{Stopwords}}

\textit{Stopwords} são palavras semanticamente irrelevantes, que sua presença
ou ausência não altera o sentido do texto. São artigos, conectivos ou
preposições.

Os principais objetivos de remover as \textit{stopwords} são retirar termos que
não influenciam diretamente o significado e diminuir o tamanho do índice.

\subsection{\textit{Natural Language Toolkit} --- NLTK}\label{sec:nltk}

%A utilização da linguagem de programação \textit{Python} é uma opção
%interessante, já que essa é uma das linguagens mais populares e de fácil uso
%atualmente. Uma das vantagens do \textit{Python} é possibilidade do uso das
%suas \textit{toolkits}, que são bibliotecas  ***********

O \glsxtrlong{nltk} (\glsxtrshort{nltk}) é uma \textit{toolkit} em
\textit{Python} utilizada para \glsxtrshort{pln} e análise de texto. Essa
biblioteca fornece classes básicas úteis para representação de dados no
\glsxtrlong{pln}, interfaces para etiquetagem de texto, \textit{tokenização},
análise sintática, interpretação semântica, classificação de texto,
\textit{stemming}, entre outras.

O uso do \glsxtrshort{nltk} é muito interessante para propósitos de
aprendizagem e criação de estratégias mais elaboradas, já que ao fornecer ao
usuário uma gama de tarefas de \glsxtrshort{pln} prontas, facilita o
desenvolvimento de problemas mais complexos.

É descrito em \textcite{loper2002nltk} a utilidade de diversos módulos
presentes no \glsxtrlong{nltk}. Alguns desses módulos definem tipos de dados e
sistemas de processamento. Os módulos de analisadores (\textit{Parsing
Modules}) produzem árvores que representam as estruturas dos textos, dentre
eles, \textit{chunkparser}, \textit{srparser}, \textit{pcfgparser} e
\textit{rechunkparser}. Os módulos de marcação (\textit{Tagging Modules})
complementam cada \textit{token} de um texto com informações como sua
\textit{synset}. O módulo \textit{classifier} classifica textos em categorias
usando métodos estatísticos.  Outra opção que o \glsxtrshort{nltk} fornece é
utilização de \textit{Córpus}\footnotemark como \textit{MacMorpho} para o
\textit{part-of-speech tagging} \cite{fonseca2015evaluating}.

\footnotetext{\textit{Córpus} são grandes corpos de
dados linguísticos sobre determinado tema \cite{bird2009natural}.}

A partir dos módulos que o \glsxtrlong{nltk} fornece é possível desenvolver
tarefas de diversos graus de dificuldade e escopo.
