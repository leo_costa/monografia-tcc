\section{Sistema de Triagem de Manchester}\label{sec:manchester}

O \glsxtrlong{stm} foi inventado no ano de 1994 na cidade de
Manchester, Reino Unido. O protocolo procura criar uma metodologia para o
processo de classificação de risco, com o objetivo de estabelecer uma
padronização no processo de triagem, de forma que, novos médicos possam
aprender a realizar o processo, seja possível realizar auditorias e criar um
consenso entre os profissionais da ala de emergência dos hospitais
\cite{livroProtocoloManchester}.

De acordo com \textcite{livroProtocoloManchester}, quando os recursos
disponíveis na ala de emergência de um hospital são limitados, a classificação
de risco é uma forma de gerenciar o atendimento dos pacientes de forma segura.
Na classificação de risco são coletados os sintomas e algumas outras
informações do paciente, de forma a classificá-lo de acordo com sua
necessidade.

No \glsxtrshort{stm}, a classificação é feita com base em 54 fluxogramas, onde,
cada fluxograma é designado para um tipo específico de sintoma/doença do
paciente, por exemplo, existe o fluxograma para dor nas costas, picadas ou
mordidas, dor abdominal entre outros. Esses fluxogramas são compostos por
diversos discriminantes, que são os sintomas que o paciente está apresentando,
por exemplo, dor intensa, pulsação baixa, hemorragia descontrolada e outros.
Os discriminantes são utilizados para determinar qual a classificação de risco
do paciente dentro do fluxograma. Essa classificação é divida em cinco níveis e
pode ser vista na Tabela \ref{tab:riscoManchester}
\cite{livroProtocoloManchester}.

\begin{table}[!htb]
  \centering
  \caption{Tabela de níveis de classificação de risco}\label{tab:riscoManchester}
  \begin{tabular}{llc}
    \toprule
    Classificação & Cor & Tempo Máximo de Espera [min] \\ \toprule
    Atendimento imediato & Vermelho & 0\\
    Muito urgente        & Laranja  & 10\\
    Urgente              & Amarelo  & 60\\
    Padrão               & Verde    & 120\\
    Não urgente          & Azul     & 240\\
    \bottomrule
  \end{tabular}
  \smallcaption{Fonte: Adaptado de \textcite{livroProtocoloManchester}.}
\end{table}

De maneira geral, o \glsxtrshort{stm} apresenta uma solução robusta no quesito
de classificação de risco, o motivo para isto é que, mesmo que seja escolhido
um fluxograma incorreto para a avaliação do paciente, devido aos discriminantes
utilizados a classificação final estará correta
\cite{livroProtocoloManchester}.

\subsection{Medição da dor do paciente}\label{sec:medDor}

Complementando a Seção \ref{sec:objetivo}, a dor também é um dos principais
problemas dos pacientes que vão a um serviço de emergência. De acordo com
\textcite{livroProtocoloManchester}, a dor é um parâmetro de grande importância
por diversos motivos, como, a influência da intensidade de dor na urgência de
atendimento, os pacientes têm uma expectativa de que sua dor seja resolvida ou
minimizada e, também, é um indicativo da qualidade percebida do serviço.

Ao analisar os fluxogramas definidos em \textcite{livroProtocoloManchester},
nota-se que a dor é um dos discriminantes mais comuns. Mais especificamente, a
dor severa aparece como discriminador do nível laranja em 44, dos 54,
fluxogramas do \glsxtrshort{stm}, isso também pode ser verificado na
\figura{fig:wcProt}, onde, a dor severa é um dos cinco discriminantes que mais
aparece.

Portanto, é indiscutível que, no contexto do \glsxtrshort{stm}, identificar a
intensidade de dor de um paciente é uma tarefa extremamente importante para uma
classificação de risco correta. No entanto, esta tarefa não é trivial, mesmo
para profissionais treinados. Em \textcite{livroProtocoloManchester}, o autor
destaca algumas técnicas comumente utilizadas que apresentam resultados
satisfatórios para aferir a intensidade de dor de um paciente, essas técnicas
serão apresentadas nas seções a seguir.

\subsubsection{Escalas Verbais}

De acordo com \textcite{livroProtocoloManchester}, essas são escalas que
utilizam, em média, de três à cinco palavras para descrever a intensidade de
dor, e, essas palavras, geralmente, estão vinculadas à uma escala numérica.
Algumas palavras comuns para descrição da dor são:

\begin{itemize}[label=$\bullet$]
  \item Nenhuma,
  \item Leve,
  \item Moderada,
  \item Severa,
  \item Agonizante
\end{itemize}

No entanto, algumas desvantagens dessa forma de descrição são: dependência da
percepção de dor do paciente, e, que uma única palavra pode não representar com
precisão a intensidade da dor \cite{livroProtocoloManchester}.

\subsubsection{Escalas Visuais}

São escalas visuais, geralmente representadas por uma linha horizontal, onde,
uma extremidade representa ausência de dor, e a outra extremidade representa um
nível de dor extremo \cite{livroProtocoloManchester}. O uso dessa escala é bem
simples, basta o paciente identificar, ao longo da linha, qual a intensidade de
dor que ele está sentindo. Um exemplo dessa escala pode ser visto na
\figura{fig:escVisual}.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{.8\textwidth} % Regula o tamanho da figura
            \centering
                \caption{Exemplo de escala visual para medição da intensidade
                de dor}\label{fig:escVisual}
                \includegraphics[width=\linewidth]{Imagens/EscalaVisual.eps}
                \smallcaption{Fonte: Adaptado de
                \textcite{livroProtocoloManchester}.}
          \end{minipage}
\end{figure}

Porém, de acordo com \textcite{livroProtocoloManchester}, alguns pacientes
acham essa escala muito abstrata para usar, ou, costumam marcar a intensidade
de dor sempre próxima das extremidades. Uma vantagem dessa escala é que ela é
mais sensível do que a escala verbal.

\subsubsection{Régua de Dor}

Embora não exista um método objetivamente melhor que o outro para identificação
da intensidade de dor, alguns métodos são mais adequados que outros dependendo
da situação. Com isso em mente, \textcite{livroProtocoloManchester} definem que
um método adequado para esta tarefa deve ser simples, rápido para utilizar,
confiável e ter sido validado.

Dadas essas características, a régua de dor é uma boa ferramenta para a tarefa
de medição de intensidade de dor, pois, combina a descrição verbal com uma
escala visual, a avaliação é rápida e simples, pode ser facilmente adaptada
para uso com crianças, entre outras vantagens \cite{livroProtocoloManchester}.
Um exemplo de régua de dor pode ser visto na \figura{fig:reguaDor}, a régua
incorpora as três intensidades utilizadas como discriminantes nos fluxogramas
do protocolo, sendo elas, dor leve (1--4), moderada (5--7) e severa (8--10),
além disso, a régua incorpora algumas descrições verbais para auxiliar o
paciente a escolher a intensidade adequada.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{.8\textwidth} % Regula o tamanho da figura
            \centering
                \caption{Exemplo de régua de dor}\label{fig:reguaDor}
                \includegraphics[width=\linewidth]{Imagens/ReguaDor.eps}
                \smallcaption{Fonte: Adaptado de
                \textcite{livroProtocoloManchester}.}
          \end{minipage}
\end{figure}
