\section{Reconhecimento de Comportamento}\label{sec:reconhecimento-comportamento}

Complementando a Seção \ref{sec:objetivo}, para a análise de comportamento dos
pacientes, pode ser utilizado um sistema de câmeras fixadas na sala de
espera. A proposta de utilização de um sistema de câmeras para esta tarefa é
fortalecida pelo estudo de \textcite{JORGE2012indicadoresdeagravamento}. O
artigo, que propõe indicadores de agravamento dos pacientes em um
pronto-socorro, enquadra o comportamento do paciente como um dos indicadores
mais utilizados pelas equipes médicas para descriminar uma piora no quadro
clinico dos pacientes.

Nos relatos presentes no estudo de
\textcite{JORGE2012indicadoresdeagravamento}, dois tipos de comportamentos que
chamam atenção da equipe de atendimento do hospital são descritos. O primeiro
comportamento listado é o caso de um paciente agitado e irritado. O segundo
caso é um paciente extremamente quieto, parado e com pouca ou nenhuma interação
com o ambiente e pessoas ao seu redor.  Além destes dois comportamentos,
\textcite{JORGE2012indicadoresdeagravamento} ainda cita a atenção com uma
possível queda do paciente. Segundo a taxonomia  de \textcite{Chaaraoui2012comportamento}, a queda é
enquadrada como uma ação e, assim como os comportamentos citados anteriormente,
também pode ser flagrada pelo sistema de câmeras.

\textcite{moura2018triagemrapida} sugere a importância da observação de uma
possível convulsão do paciente na sala de emergências dos hospitais. A
convulsão seria classificada como uma atividade, segundo a taxonomia definida
para este trabalho. Entretanto, nos resultados de seu próprio artigo, a autora
classifica esta análise como a que possui menor grau de relevância entre todas
as propostas em seu estudo. Tal atividade também poderia ser capturada ao
analisar a queda do paciente, dessa forma, estaria ao menos parcialmente
encoberta pelo sistema.

É importante ressaltar que um comportamento, segundo
\textcite{Chaaraoui2012comportamento}, pode ser analisado a partir de horas ou
até semanas, porém, segundo \textcite{livroProtocoloManchester}, um paciente
não deve permanecer na espera de atendimento por mais de 240 minutos, mesmo se
classificado como não urgente. A solução para essa divergência será trabalhada
de maneira mais profunda posteriormente neste trabalho.

\subsection{Sistemas de monitoramento de comportamento}

\textcite{skinner2003comportamento} define, em seu livro
``\textit{\citefield{skinner2003comportamento}{title}}'', o comportamento como
um processo mutável, fluido e evanescente, tornando-se, assim, algo complexo e
que, portanto, exige uma alta demanda de energia e engenhosidade de um
cientista ao tentar descrever uniformidades adequadas ou relações ordenadas
sobre o tema.

Segundo \textcite{MOESLUND2006comportamento}, entre os anos de 2000 e 2006
foram publicados mais de 350 artigos sobre análise e percepção de movimentações
humanas baseadas em visão computacional e entendimento de imagem. Tal movimento proporcionou
diversos artigos com significante avanço e, também, trouxe
progresso para áreas como o estudo do comportamento e o reconhecimento
automático de ações humanas.

\textcite{MOESLUND2006comportamento} propõem um modelo para os sistemas desta
área de estudo, quando baseados em visão computacional, segmentado em quatro
principais passos. Para o autor torna-se necessário, primeiramente, uma
inicialização do sistema para assegurar que o sistema estará analisando o
sistema atual e não qualquer vestígio de um sistema anterior.  Secundariamente,
o rastreamento é utilizado para segmentar e rastrear os objetos que devem ser
analisados de fato, neste caso, os humanos. Após o rastreamento, torna-se
necessário estimar a atual pose do humano em um ou mais quadros de imagem. Por
fim, o sistema estará preparado para a tarefa de reconhecimento do movimento,
ação, atividade ou comportamento.

\subsubsection{Inicialização}

De acordo com \textcite{Moeslund2001}, a inicialização é o conjunto de ações ou
um processo específico inicial para garantir que todos outros sistemas estão
alinhados e orientados corretamente para começar a análise do cenário atual.
Segundo os autores, este processo está, geralmente, relacionado com a calibração
das câmeras, adaptação das características do cenário e a inicialização de um
modelo.

\paragraph{Calibração de câmeras}

A calibração das câmeras é um processo importante e, para sua execução,
necessita-se saber os parâmetros das câmeras que estão sendo utilizadas. Na
maioria dos casos, o processo de calibração ocorre sem a necessidade de uma
conexão com a rede, porém, em certos casos onde o procedimento de \textit{setup}
possui mudanças constantes pode-se utilizar algum tipo de calibração \textit{online}
\cite{Moeslund2001}.

\paragraph{Adaptação às características do cenário}

A fração da inicialização correspondente à adaptação às características do
cenário está muito relacionada às hipóteses assumidas em relação ao ambiente.
Outro processo conectado a essa etapa é o sistema de segmentação das imagens
\cite{Moeslund2001}.

Para casos em que o sistema é, completamente, baseado em hipóteses,
normalmente, apenas um procedimento \textit{off-line} inicial é capaz de
distinguir as referências utilizadas posteriormente na etapa de processamento.
Em sistemas adaptativos, este passo captura parâmetros necessários para
realização de cálculos e atualizações do processo \cite{Moeslund2001}.

\paragraph{Inicialização do modelo}

O passo de inicialização do modelo é diretamente relacionado a dois outros
procedimentos: a pose inicial do objeto e o modelo de representação deste mesmo
objeto. Ambos os procedimentos estão inseridos no sistema de estimativa de pose
que será trabalhado de forma mais profunda posteriormente neste artigo
\cite{Moeslund2001}.

Um problema recorrente entre os pesquisadores enquanto tentam criar sistemas de
estimativa de pose é, justamente, encontrar a pose inicial do humano. Sendo
assim, a etapa de inicialização do modelo é utilizada para contornar este
problema indicando, na maior parte das vezes, manualmente a posição do
individuo e, com isso, as decorrentes fases do sistema tomam como conhecida
a pose inicial. São poucos os modelos que possuem um sistema automático para
reconhecer a pose inicial e fogem deste processo \cite{Moeslund2001}.

Em outro artigo, \textcite{MOESLUND2006comportamento} relata que o processo de
inicialização captura conhecimentos prévios sobre um individuo específico e,
que, tais conhecimentos seriam necessários em outras etapas como na
inicialização estrutura cinemática, na construção da projeção do corpo em 3D,
na inicialização de aparências, na etapa de pose e, por fim, na etapa de tipos
de movimentos.

\subsubsection{Rastreamento}

A área de estudo relacionada ao rastreamento é vasta e pode ser abordada de
diversas maneiras. Para este aspecto do estudo, o processo de rastreamento será
utilizado como parte integrante de outro sistema, assim como na Seção
\ref{sec:reconhecimento-facial}.  Porém, na Seção \ref{sec:localizacao} o
rastreamento será abordado como um sistema independente \cite{Moeslund2001}.


\textcite{zhang2017review} relata a intensa aplicação dos métodos de
rastreamento em sistemas de vigilância para encontrar um possível comportamento
estranho. Para os autores, o rastreamento é definido como algo que deve
localizar uma pessoa ao longo de um determinado trecho de vídeo. No artigo
são abordados dois tipos de métodos para rastreamento: o rastreamento baseado
em filtros, focado no filtro de Kalman, e o rastreamento baseado em trechos,
onde o sistema observado é analisado em blocos separados.

Segundo \textcite{Moeslund2001} se o rastreamento foi desenhado para preparar
os dados para o sistema de estimativa de pose seu objetivo é extrair
informações específicas de uma imagem como membros do corpo. Caso o sistema
seja utilizado para preparar os dados para o reconhecimento, seu papel é
representar os dados de alguma maneira específica que facilitará o processo de
reconhecimento.

Para \textcite{Moeslund2001} independentemente do contexto utilizado, este
processo é, normalmente, fragmentado em três aspectos: segmentação do plano da
figura, representação dos dados e rastreamento ao longo do tempo.

\paragraph{Segmentação do plano da figura}

Segundo \textcite{MOESLUND2006comportamento}, o processo de segmentação do
plano da figura consiste em separar o objeto interessado, neste caso o humano,
do resto da imagem, chamado de plano de fundo.


\textcite{Moeslund2001} divide as bases da segmentação do plano da figura em
dois tipos principais.

\begin{enumerate}[label=$\bullet$]
    \item \textbf{Dados temporais:} Os dados temporais estão,
      geralmente, conectados aos sistemas onde o plano de fundo e o ponto de
      visão (câmeras) estão estáticos. Neste caso, as diferenças entre um
      quadro e outro devem originar os movimentos do objeto. Existem duas
      subclasses aplicadas dentro destes sistemas: a subtração e o fluxo
      \cite{Moeslund2001}.

    A subtração é o processo de subtrair a imagem atual pelas imagens
    antecessoras em um processo de \textit{pixel} a \textit{pixel}. O resultado
    dessa subtração deve ser composto pelo movimento do objeto e algum ruído.
    Este processo possui uma inicialização simples, pois consiste em gravar um
    quadro do plano de fundo sem nenhuma ocorrência de ruído ou objeto
    \cite{Moeslund2001}.

    O fluxo é definido por \textcite{Moeslund2001} como um termo referente a
    análise da movimentação coerente dos pontos ao decorrer dos quadros. Ou
    seja, é a avaliação da locomoção de certos pontos ao decorrer de um
    período.

    \item \textbf{Dados espaciais:} O uso dos dados espaciais está relacionado
      a duas subclasses: os métodos limiares e métodos estatísticos. Os métodos
      limiares baseiam-se na aplicação de hipóteses, como exemplo, cita-se o
      \textit{Chroma-key} onde, partindo do pressuposto que tudo que é de uma
      determinada cor é o plano de fundo, o objeto é estratificado do plano de
      fundo, pois possui uma coloração diferente.  Os métodos estatísticos
      estão, normalmente, relacionados a utilizar critérios de variância
      juntamente com o conceito de subtração em certos grupos de \textit{pixel}
      para diferenciar o plano de fundo do objeto a ser observado
      \cite{Moeslund2001}.
\end{enumerate}

\paragraph{Representação}

A representação consiste em uma forma de transformar os dados obtidos através
das câmeras em algum formato específico para ser analisado pelo método. No caso
dos sistemas que já utilizaram o conceito de segmentação do plano de imagem é
comum a utilização de uma representação baseada no próprio objeto como é o caso
da representação das silhuetas de um corpo humano tanto em 2D quanto em 3D. Já
nos casos onde a segmentação ainda não ocorreu ou, até, não ocorrerá aplica-se
uma representação baseada diretamente na imagem. Para as representações
baseadas em imagem, normalmente, aplicam-se transformações nos dados dos
\textit{pixels} da imagem, como exemplo, a transformada de Fourier e a
\glsxtrshort{dct}.  \cite{Moeslund2001}.

\paragraph{Rastreamento ao longo do tempo}

Esse é um processo contínuo que busca encontrar correspondências do objeto a
ser observado a cada quadro de imagem e verificar, assim, a sua trajetória. As
dificuldades encontradas nessa etapa estão diretamente relacionadas ao nível de
complexidade do próprio objeto e do cenário a ser analisado, quanto maior o
\glsxtrshort{dos}, maior a dificuldade. Ruídos e oclusões podem direcionar o
sistema a um rastreamento equivocado e para superar tais dificuldades muitas
vezes um sistema de predição é utilizado para comparar a posição estimada com a
posição real e, assim, obter uma situação de contorno, principalmente, em casos
de oclusões \cite{Moeslund2001}.

\subsubsection{Estimativa de pose}

\textcite{Moeslund2001} referem-se a estimativa de pose como um processo de
identificação do como o corpo humano inteiro ou até alguma parte específica
dele, como os membros, está arranjado em determinada cena. Tal etapa pode ser
utilizada como um processo individual após o rastreamento ou, em alguns
sistemas, como uma etapa ativa no rastreamento.

Ainda segundo \textcite{Moeslund2001}, esse processo pode ser realizado em
níveis diferentes de precisão. Assim como pode ser usado apenas para estimar
o centro de massa ou apenas um membro do corpo humano, o processo, também,
pode ser utilizado em um nível muito mais exigente de precisão para estimar
tamanho de membros, orientação, posição e altura de determinado objeto.

Em \textcite{MOESLUND2006comportamento} a estimativa de pose é descrita
como a estimativa de configuração da base cinemática ou da estrutura de
articulação do esqueleto de uma pessoa. Assim como em \textcite{Moeslund2001}
esse artigo segmenta esse sistema em três categorias.

\begin{enumerate}[label=$\bullet$]
    \item \textbf{Modelagem livre}: Nesta categoria estão
      classificados os sistemas que não possuem um modelo prévio. Entretanto,
      nesta etapa os sistemas constroem algum tipo de modelo para representar a
      pose do objeto.  Alguns métodos utilizam representações como pontos,
      geometrias padronizadas e até figuras de palitos \cite{Moeslund2001}.

    Os estudos realizados com foco neste método, normalmente, estão voltados
    para modelos em 2D ou a criação de modelos 3D a partir de um mapeamento de
    sequências de modelos 2D \cite{MOESLUND2006comportamento}.

    \item \textbf{Modelagem indireta}: Diferente do primeiro caso, para esta
      categoria os métodos possuem um modelo prévio durante a estimativa de pose
      de um objeto. Além disso, estes métodos utilizam uma referência para
      extrair informações relevantes que guiarão as interpretações dos dados
      obtidos \cite{Moeslund2001}.

     \textcite{MOESLUND2006comportamento} relatam que os métodos desta categoria
     podem ser baseados na comparação das proporções de membros do corpo humano
     e até no reconhecimento de pose.

     \item \textbf{Modelagem direta}: A modelagem direta indica que
       o método utiliza um modelo humano prévio para representar o objeto
       observado. Esse modelo é atualizado constantemente a partir das
       observações e, com isso, é utilizado como fonte para fornecer as
       informações necessárias a qualquer instante. Tal modelagem permite
       contornar situações como oclusões no sistema de imagem
       \cite{Moeslund2001}.

     Os modelos enquadrados como modelagem direta estão conectados a uma
     geometria 3D e utilizam, em sua maioria, o método de análise a partir de
     sínteses para aperfeiçoar a similaridade entre o modelo projetado e o
     objeto observado \cite{MOESLUND2006comportamento}.
\end{enumerate}

A forte conexão entre a estimativa de pose e a técnica de segmentação
utilizada no processo de rastreamento é abordada por
\textcite{Chaaraoui2012comportamento}. No artigo são abordados trabalhos em
que a técnica de estimativa de pose não utiliza uma prévia segmentação e,
posteriormente, os autores relatam que a maior dificuldade encontrada por tais
pesquisas é, justamente, encontrar as juntas do objeto observado, passo que
seria facilitado pelo processo de segmentação.

\subsubsection{Reconhecimento}

Segundo \textcite{MOESLUND2006comportamento} a área da representação e de
reconhecimento de ações e atividades, apesar de ser uma área relativamente
velha, ainda segue imatura. Sendo uma área completamente aberta, existem
diversos tipos de pesquisa e cada uma possui uma ideia diferente e focada
exclusivamente em seu propósito.

O reconhecimento de ações é enquadrado como um tipo de pós-processamento. Este
sistema é encarregado de classificar um movimento capturado em algum dos
diversos tipos de ações abordadas pelo sistema. Essas ações podem ser das mais
simples como caminhar ou correr até as ações mais complexas como passos de
danças \cite{Moeslund2001}.

Existem dois tipos diferentes de reconhecimento. O primeiro é o reconhecimento
por reconstrução, onde o conceito aplicado é: primeiro reconstruir a cena para
depois reconhecê-la. O segundo caso é chamado de reconhecimento direto, onde
existe a tentativa instantânea de reconhecimento. A segunda opção requer um
menor nível de pré-processamento em relação à primeira, porém, não existe na
literatura um destaque maior para nenhum dos dois lados \cite{Moeslund2001}.

Outras formas de dividir os tipos de técnicas aplicadas para a etapa de
reconhecimento podem considerar o número de quadros analisados por vez, e, se o
reconhecimento é dinâmico ou estático \cite{Moeslund2001}.

\begin{enumerate}[label=$\bullet$] \item \textbf{Reconhecimento Estático:} O
    reconhecimento estático é baseado no estudo quadro a quadro. Os métodos,
    normalmente, comparam a imagem atual com um banco de imagens para buscar
    uma classificação. A informação pode ser em forma de \textit{templates},
    \textit{templates} transformados, silhuetas normalizadas ou posturas. A
    meta deste método é o reconhecimento de posturas, como exemplo, uma pessoa
    em pé ou sentada \cite{Moeslund2001}.

    \item \textbf{Reconhecimento Dinâmico:} O reconhecimento
      dinâmico utiliza características temporais para classificar a atividade
      analisada. Atividades simples, como, caminhar, são utilizadas como cenários
      de teste para esta modalidade.

      Os sistemas podem ser criados desde uma baixa complexidade de processamento até um alto nível.
      Normalmente, os casos que requerem um nível mais sofisticado são os que
      envolvem a estimativa de pose em certos membros do corpo
      \cite{Moeslund2001}.

\end{enumerate}

\textcite{Chaaraoui2012comportamento} citam a importância de envolver e
reconhecer objetos relacionados a determinada ação, pois, a interação da pessoa
observada com objetos ao seu redor e o reconhecimento destes objetos podem
auxiliar a classificação do tipo de ação ou atividade realizada.

A etapa de reconhecimento é a última etapa do ciclo definido por
\textcite{Moeslund2001} e a partir de um grupo de respostas desse sistema é
possível reclassificar essas ações em níveis superiores dependendo da
taxonomia aplicada.

\paragraph{Taxonomia}

Uma vez que o termo taxonomia é definido, por \textcite{BICUDO2004taxonomia},
como a ciência da identificação. A complexidade do estudo do comportamento
humano é realçada em ``\textit{\citefield{Chaaraoui2012comportamento}{title}}''
de \textcite{Chaaraoui2012comportamento}, após relato dos autores sobre a
variedade e a divergência de taxonomias aplicadas ao campo da Análise do
Comportamento Humano (\glsxtrshort{hba}--\glsxtrlong{hba}) pelos pesquisadores.

\textcite{Chaaraoui2012comportamento} relatam em seu artigo
a diversidade de taxonomias aplicadas nesta área de estudo.
Como exemplo, \textcite{MOESLUND2006comportamento} propõem uma divisão das
ações na seguinte escala: ações motoras primitivas, ações, e atividades.

Porém, para este estudo, toma-se como referência a taxonomia proposta por
\textcite{Chaaraoui2012comportamento} que foi baseada nas diversas taxonomias
estudadas pelos autores. Esta taxonomia propõe a segmentação da
\glsxtrshort{hba} em quatro tarefas: movimento, ação, atividade e
comportamento. Estas tarefas são classificadas de acordo com o grau de
semântica (\glsxtrshort{dos}) e a quantidade de tempo envolvida na análise de
reconhecimento e de classificação do processo. A \figura{fig:tarefasHBA}
ilustra a classificação das tarefas segundo tal taxonomia.

\begin{figure}[!htb]
      \centering
          \begin{minipage}{.73\textwidth} % Regula o tamanho da figura
                \caption{Classificação das tarefas da HBA}\label{fig:tarefasHBA}
                \includegraphics[height=0.37\linewidth]{Imagens/ClassHAB2.eps}
                \smallcaption{Fonte: Adaptado de
                \textcite{Chaaraoui2012comportamento}.}
          \end{minipage}
\end{figure}

Na camada de ``Movimentos'' tarefas como percepção de
movimentação, extração do plano de fundo e segmentação são abrangidas. Tais
tarefas possuem um curto período de observação para análise, cerca de alguns
quadros de imagem \cite{Chaaraoui2012comportamento}.

A camada ``Ações'' trata do reconhecimento dos movimentos
realizados por uma pessoa e, também, com quais objetos tal pessoa pode estar
interagindo. Alguns exemplos de ações são sentar, levantar, deitar e andar. O
período de tempo está geralmente na casa das unidades de segundos.
\cite{Chaaraoui2012comportamento}.

As ``Atividades'' são definidas com uma composição de uma série de ações. Nesse
nível, o período observado parte da casa de dezenas de segundo para algumas
unidades de minuto e as atividades do dia a dia (\glsxtrshort{adl}) são
enquadradas. Como exemplo de atividades, é possível citar: cozinhar, limpar a
casa e tomar banho \cite{Chaaraoui2012comportamento}.

Por fim, na camada superior da pirâmide, portanto, com maior grau de
complexidade semântica, \textcite{Chaaraoui2012comportamento} utiliza o
termo ``Comportamento''. Esta camada pode ter um período de observação de
horas, dias ou até semanas para que os padrões do estilo de vida, rotina e
hábitos pessoais sejam de fato entendidos e possam ser comparados com
anormalidades.
