\section{Reconhecimento de Dor}\label{sec:rec-dor}

De acordo com \textcite[p. 1637]{enciclopediaDor} a dor é definida pela
Associação Internacional para o Estudo da Dor
(\glsxtrlong{iasp}--\glsxtrshort{iasp}) como sendo ``uma experiência sensorial
e emocional desagradável com atual, ou potencial, dano aos tecidos, ou,
descrita em termos de tais danos''. Embora a definição seja clara em relação à
como a dor se manifesta/é percebida, o processo de medição da presença e até
mesmo a intensidade da dor é algo subjetivo e que depende de diversos
fatores, tanto por parte do médico responsável por realizar a medição, quanto
do paciente que está relatando sua dor \cite{livroProtocoloManchester}.

%Como pode ser visto em \textcite{livroProtocoloManchester}, a dor é um dos
%discriminantes mais comuns nos fluxogramas do protocolo de Manchester e, além
%disso, é um discriminante que, dependendo da intensidade, pode fazer com que a
%classificação do paciente suba dos níveis mais baixos de prioridade (verde,
%amarelo), para uma classificação que implica um atendimento quase que imediato,
%como é o caso da classificação laranja ou vermelha.

%Além disso, no caso do atendimento de um paciente no sistema de emergência de
%um hospital, a dor é um dos problemas mais comuns entre os pacientes, e,
%dependendo da intensidade, o paciente sob dor pode causar problemas tanto para
%a equipe do hospital, quanto para outros pacientes
%\cite{livroProtocoloManchester}.

Como já apresentado na Seção \ref{sec:medDor}, a capacidade de identificação da
presença de dor num paciente, e até mesmo a intensidade dessa dor, é uma
habilidade essencial para o agente responsável por monitorar o quadro clínico
de um paciente, especialmente quando deseja-se utilizar o \glsxtrshort{stm},
onde a intensidade de dor do paciente é um importante discriminante. No
entanto, os métodos apresentados para medição de dor (ver Seção
\ref{sec:medDor}) impossibilitam uma medição contínua da intensidade de dor do
paciente por diversos motivos, como, sobrecarga das atividades que o médico
deve realizar \cite{zhou2016recurrent}.

Porém, dados os avanços nas técnicas de \glsxtrshort{ia} e \glsxtrshort{ml},
diversas pesquisas foram feitas com o objetivo de criar sistemas autônomos
capazes de inferir, com precisão e robustez, a presença e até mesmo a
intensidade da dor em seres humanos, a partir das expressões
faciais. Um breve estudo sobre a literatura pode ser visto nas seções a seguir.

\subsection{Técnicas para Reconhecimento de Dor}

Como dito anteriormente, devido às dificuldades em medir a intensidade de dor,
diversas pesquisas foram feitas com o objetivo de criar algoritmos capazes de
realizar a detecção da dor do paciente de forma automática e não invasiva. É
possível separar essas pesquisas em dois grupos, onde, o primeiro realiza a
classificação da dor de forma indireta, ou seja, baseada nas micro expressões
faciais
\cite{lucey2009automatically,lucey2010automatically,lucey2012painful,virrey2019visual,ashraf2009painful},
enquanto outras procuram realizar a classificação direta
\cite{roy2016approach,hammal2012automatic,walter2013biovid,velana2016senseemotion,werner2014comparative,rodriguez2017deep}.
Como pode ser visto, essa é uma área de grande interesse, pois, embora existam
métodos relativamente confiáveis para realizar esta tarefa manualmente, como a
régua de dor proposta no \glsxtrshort{stm} \cite{livroProtocoloManchester},
esses métodos verbais e/ou visuais só funcionam com pessoas capazes de se
comunicar com o médico avaliador e demandam tempo e recursos que talvez não
estejam disponíveis \cite{lucey2012painful}. Portanto, nas seções a seguir será
feita uma análise das referências aqui apontadas, de forma a entender o
processo por trás do reconhecimento de dor através da expressão facial.

Embora os trabalhos vistos em
\textcite{walter2013biovid,velana2016senseemotion} não sejam estudos
específicos para a classificação de dor, ambos descrevem em detalhes o processo
para obtenção de bases de dados baseadas em dor induzida por calor. Ambas as
bases são multi modais, isto é, são armazenados diversos sinais do paciente com
o objetivo de identificar a dor, alguns desses sinais são \glsxtrlong{eeg}
(\glsxtrshort{eeg}), \glsxtrlong{ecg} (\glsxtrlong{ecg}), Nível de Condutância
da Pele (\glsxtrlong{scl}--\glsxtrshort{scl}), vídeos, áudio entre outros. As
bases de dados também possuem informações a respeito da intensidade de dor
percebida pelos voluntários.

\subsubsection{Reconhecimento Indireto da Dor --- Baseado em Unidades de
Ação}\label{sec:recDorInd}

Segundo \textcite{virrey2019visual}, métodos baseados na detecção de unidades
de ação (\glsxtrlong{au}--\glsxtrshort{au}), são capazes de apresentar bons
resultados na detecção da intensidade de dor. As \glsxtrshort{au} fazem parte
do Sistema de Codificação de Ações Faciais
(\glsxtrlong{facs}--\glsxtrshort{facs}), esse sistema mapeia todos os
movimentos visualmente identificáveis causados pelos músculos da face.  As
expressões faciais são decompostas nesses movimentos, e, cada movimento é uma
unidade de ação (\glsxtrshort{au}) \cite{ekman1977facial}.

Dessa forma, utilizando o \glsxtrshort{facs}, \textcite{virrey2019visual}
aponta que é possível medir a intensidade de dor a partir da escala de Prkachin
e Solomon (\glsxtrlong{pspi}--\glsxtrshort{pspi}), definida na
\equacao{eq:PSPI}.

\begin{multline}\label{eq:PSPI}
  Dor_{\text{PSPI}} = Intensidade(\text{AU4}) + \max(Intensidade(\text{AU6,AU7})) \\
  + \max(Intensidade(\text{AU9,AU10})) + Intensidade(\text{AU43})
\end{multline}

Onde, \glsxtrshort{au}4 é a ação de abaixar a sobrancelha, \glsxtrshort{au}6 e
\glsxtrshort{au}7 indicam contração do músculo orbicular dos olhos, essas são
ações que ocorrem ao fechar os olhos ao sorrir, por exemplo. \glsxtrshort{au}9
e \glsxtrshort{au}10 são as ações que causam enrugamento na região do nariz e
elevação do lábio superior, respectivamente, e \glsxtrshort{au}43 indica
fechamento total dos olhos. Todas as \glsxtrshort{au}, com exceção da
\glsxtrshort{au}43, que é binária, são medidas numa escala de zero à
cinco\footnote{Na notação comum do \glsxtrshort{facs} a intensidade é indicada
utilizando caracteres de A à E, onde, A indica intensidade 1 e E indica
intensidade 5}, onde zero indica ausência da \glsxtrshort{au} e cinco indica
intensidade máxima \cite{virrey2019visual}. Dadas as intensidades máximas para
cada \glsxtrshort{au}, nota-se que a escala \glsxtrshort{pspi} possui uma
variação de intensidade que vai de 0 à 16. Um exemplo da localização dessas
\glsxtrshort{au}s pode ser visto na \figura{fig:ex_aus_dor}.

\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.6\textwidth} % Regula o tamanho da figura
        \caption{Exemplo das \glsxtrshort{au}s utilizadas no reconhecimento de
        dor.}\label{fig:ex_aus_dor}
        \smallcaption{Legenda: Neste exemplo, a intensidade de dor
        \glsxtrshort{pspi} é $\text{PSPI} = 4 + \max(3,4) + \max(2,3) + 1 = 12$.}
        \includegraphics[width=0.9\linewidth]{Imagens/pain_aus.png}
        \smallcaption{Fonte: \textcite{virrey2019visual}.}
      \end{minipage}
\end{figure}

Existem diversas bases de dados para a detecção dessas \glsxtrshort{au} e
também da dor do indivíduo, alguns exemplos são: UNBC-McMaster \textit{Shoulder
pain expression archive}, \textit{Biopotential and Video} (BioVid) \textit{heat
pain database}, \textit{Cohn-Kanade} (CK) e \textit{Extended} CK (CK+)
\textit{database}, \textit{Affectiva-MIT facial expression dataset} (AM-FED) e
\textit{Man-Machine Interaction} (MMI) \textit{facial expression database}.
Detalhes à respeito dessas bases de dados podem ser vistos em
\textcite{virrey2019visual}. Vale a pena destacar que a base de dados
UNBC-McMaster é uma das mais utilizadas nos trabalhos feitos nessa área de
reconhecimento de dor. Ela possui imagens/vídeos de pacientes no estado de
dor/não dor, com a identificação das \glsxtrshort{au} do paciente
\textit{frame} à \textit{frame}. A identificação das \glsxtrshort{au} nas
imagens foi feita por profissionais treinados e o consenso à respeito das
classificações foi de $95\%$ entre os profissionais \cite{lucey2012painful}.

Um outro trabalho baseado no \glsxtrshort{facs} poder ser visto em
\textcite{lucey2012painful}, nele, foi feito o uso da técnica de
\glsxtrlong{svm} (\glsxtrshort{svm}) para a classificação das \glsxtrshort{au}s
e também da intensidade de dor. O autor fez uma investigação à respeito da
detecção de dor utilizando não somente as intensidades das \glsxtrshort{au}s
relacionadas à dor (ver \equacao{eq:PSPI}), mas também da movimentação do
paciente durante a experiência de dor, mais especificamente, foi analisada a
movimentação da cabeça. Os resultados obtidos no trabalho apontam que, embora a
detecção do movimento da cabeça influencie na detecção de níveis mais elevados
de dor, nos casos onde a dor é menos intensa não há uma grande correlação.
Além disso, também foi observado que a movimentação da cabeça durante a
experiência de dor foi algo dependente do indivíduo, ou seja, alguns movimentam
a cabeça ao sentir dor e outros não.

O trabalho feito por \textcite{lucey2012painful} também compara o desempenho de
uma \glsxtrshort{svm} para classificação da dor do paciente de acordo com a
Intensidade de Dor classificada por Observadores
(\glsxtrlong{opi}--\glsxtrshort{opi}), que é uma escala de dor que foi
atribuída por profissionais experientes que analisaram as imagens/vídeos da
base de dados McMaster-UNBC, essa escala varia de 0 à 5. Os resultados
demonstram que o algoritmo apresentou uma boa capacidade de classificação para
as intensidades 0 e 1, porém, para intensidades maiores o desempenho não foi
tão bom.

%TODO: Criar/achar o bibtex para a tese do L. Buzzuti
%TODO: Colocar em um lugar melhor
%Como qualquer algoritmo de identificação de padrões a partir de expressões
%faciais, o primeiro passo é encontrar a face e recortá-la da imagem completa
%[L. Buzzuti*****]. Isso é feito para que os algoritmos de aprendizado não façam
%a classificação baseado em parâmetros não relacionados à face, e também para
%que o conjunto de dados de entrada tenha uma dimensão menor. Com isso em mente,
%é importante notar que em \textcite{lucey2012painful} é feito o uso de um
%Modelo de Aparência Ativo (\glsxtrlong{aam} --\glsxtrshort{aam}) para extrair
%somente a região da face do paciente da imagem.

% lucey -> ~80% AUC
\textcite{lucey2009automatically} apresenta uma comparação entre formas diretas
e indiretas de medição de dor, isto é, a forma direta implica que o algoritmo
de classificação deverá identificar diretamente a presença e/ou intensidade da
dor, já a forma indireta implica que o algoritmo de classificação deverá
identificar, por exemplo, as \glsxtrshort{au}s responsáveis por definir a
presença e intensidade da dor. Assim como os trabalhos anteriores, o autor
também utilizou a base de dados McMaster-UNBC. Os resultados obtidos neste
trabalho mostram que a detecção indireta, por meio da detecção das
\glsxtrshort{au}s, gerou melhores resultados em comparação com a detecção
direta.

Além disso, os trabalhos vistos em
\textcite{lucey2009automatically,lucey2010automatically} e
\textcite{lucey2012painful}, comparam de dois a três parâmetros extraídos do
Modelo de Aparência Ativo (\glsxtrlong{aam}--\glsxtrshort{aam})
\cite{cootes2001active,matthews2004active} para o treinamento dos modelos de
classificação. Os resultados mostram que, em relação à classificação das
\glsxtrshort{au}s, cada parâmetro possui vantagens para detectar certos tipos
de \glsxtrshort{au}, por exemplo, a \glsxtrshort{au}43 foi melhor detectada
pela superfície extraída pelo \glsxtrshort{aam}, que é uma representação
geométrica. Portanto, foi proposto o uso de uma fusão através de uma regressão
logística linear para combinar os melhores modelos para identificação das
\glsxtrshort{au}s. De forma análoga, com a melhora na detecção das
\glsxtrshort{au}s, a detecção da intensidade da dor também melhorou.

Uma questão que surge ao realizar a classificação de dor a partir de vídeos é
se esta análise deve ser feita \textit{frame} a \textit{frame} ou se ela pode
ser feita para uma sequência de \textit{frames}. Da segunda maneira, é mais
simples de construir as bases de dados para o treinamento dos algoritmos, pois
não seria necessário que o profissional com domínio do \glsxtrshort{facs}
analisasse todos os \textit{frames} \cite{ashraf2009painful}. No entanto, o
trabalho feito por \textcite{ashraf2009painful} mostra uma comparação entre
essas duas formas de classificação para a base de dados McMaster-UNBC, e, os
resultados demonstram que os classificadores baseados na análise \textit{frame}
a \textit{frame} apresentam resultados consideravelmente melhores do que os que
foram treinados da outra maneira.

Em relação aos métodos utilizados para classificação, o trabalho visto em
\textcite{ashraf2009painful} adota as mesmas técnicas já discutidas até o
momento, ou seja, também é feito o uso de um \glsxtrshort{aam} para extração
dos parâmetros utilizados no treinamento e de uma \glsxtrshort{svm} para
classificação da dor.

Em comparação aos trabalhos apresentados anteriormente nesta seção, o trabalho
visto em \textcite{kaltwang2012continuous} é o que realiza a extração de
parâmetros de forma mais diferenciada. Neste trabalho, o autor utiliza os
pontos de fiduciais da face (um exemplo pode ser visto na
\figura{fig:pts_face}), os primeiros 500 coeficientes da Transformada Discreta
dos Cossenos (\glsxtrlong{dct}--\glsxtrshort{dct}) \cite{ahmed1974discrete} e
os histogramas obtidos com a técnica de Padrões Binários Locais
(\glsxtrlong{lbp}--\glsxtrshort{lbp}) \cite{ojala2002multiresolution}. A partir
disso, foram realizadas dois tipos de classificação de dor, a primeira é uma
classificação indireta, onde, foram estimadas as intensidades das
\glsxtrshort{au}s utilizando \glsxtrlong{rvr} (\glsxtrshort{rvr}) e a
intensidade de dor foi calculada utilizando a \equacao{eq:PSPI}. A segunda é
uma classificação direta, onde, são treinadas três \glsxtrshort{rvr}, uma para
cada tipo de parâmetro, essas \glsxtrshort{rvr} estimam, de forma independente,
a intensidade de dor, e então, a intensidade final é dada de duas maneiras:
calculando a média entre elas, ou, utilizando uma outra \glsxtrshort{rvr} para
fazer a fusão dos resultados calculados. Para a forma direta foram obtidos bons
resultados, os dados do autor mostram que os modelos atingiram um erro médio
quadrático (\glsxtrlong{mse}--\glsxtrshort{mse}) de $1.373$, que é um valor
baixo em comparação com o que foi encontrado na literatura.


\begin{figure}[!htb]
      \centering
      \begin{minipage}{0.55\textwidth} % Regula o tamanho da figura
        \caption{Exemplo dos pontos fiduciais da face.}\label{fig:pts_face}
        \smallcaption{Legenda: Os pontos em vermelho são os pontos fiduciais,
        no total são 64.}
        \includegraphics[width=0.7\linewidth]{Imagens/aam_model.png}
        \smallcaption{Fonte: Autor.}
      \end{minipage}
\end{figure}

Os resultados obtidos em \textcite{kaltwang2012continuous} demonstram que a
combinação dos parâmetros da \glsxtrshort{dct} com o \glsxtrshort{lbp}
utilizando a fusão por \glsxtrshort{rvr} apresentou o melhor resultado para
estimar a intensidade de dor de forma indireta.  De acordo com o autor, a
partir da \glsxtrshort{dct} é possível extrair parâmetros que descrevem
variações globais da face, enquanto que, utilizando o \glsxtrshort{lbp} é
possível extrair parâmetros que descrevem variações locais da face. Por esses
motivos, essa combinação de parâmetros consegue representar melhor as mudanças
na expressão facial do paciente sob dor.

Um comportamento importante apontado por \textcite{kaltwang2012continuous} é
que, o algoritmo aprendeu que a ação de fechar os olhos indicam a presença de
dor, porém, como a análise foi feita \textit{frame} a \textit{frame}, ocorreram
picos de dor quando o paciente estava somente piscando os olhos. Isso ocorre
devido ao fato que, o algoritmo não tem a capacidade de diferenciar entre a
ação de piscar os olhos e fechá-los devido à dor. Portanto, para evitar esse
tipo de imprecisão, é necessário incluir na análise, parâmetros à respeito da
movimentação da face do paciente, já que uma piscada é algo que ocorre num
curto espaço de tempo, diferente de fechar os olhos devido à dor.

\subsubsection{Reconhecimento Direto da Dor}

De forma similar aos trabalhos apresentados na Seção \ref{sec:recDorInd}, em
\textcite{hammal2012automatic} a classificação da intensidade da dor também é
baseada na escala \glsxtrshort{pspi}, porém, o algoritmo é treinado para
identificar diretamente a intensidade da dor, e não a intensidade das
\glsxtrshort{au}s para uso na \equacao{eq:PSPI}. Neste trabalho, também foi
utilizada a base de dados McMaster-UNBC, porém, a escala \glsxtrshort{pspi} que
varia de 0 a 16, foi quantizada em quatro classes, como mostra a lista a
seguir.

\begin{itemize}
  \item[$\bullet$] \glsxtrshort{pspi} = 0 $\rightarrow$ Não há dor
  \item[$\bullet$] \glsxtrshort{pspi} = 1 $\rightarrow$ Traços de dor
  \item[$\bullet$] \glsxtrshort{pspi} = 2 $\rightarrow$ Dor fraca
  \item[$\bullet$] \glsxtrshort{pspi} $\ge3$ $\rightarrow$ Dor forte
\end{itemize}

De acordo com \textcite{hammal2012automatic}, esse agrupamento foi feito pois
existem poucos dados para níveis de dor com intensidade maior que 3 na escala
\glsxtrshort{pspi}. Para a classificação da intensidade da dor, também foi
utilizado o método de \glsxtrshort{svm}. No total, foram treinadas quatro
\glsxtrshort{svm}s, sendo uma para cada classe de dor, definidas anteriormente.
A maior diferença entre este trabalho e o que foi apresentado por
\textcite{lucey2012painful} é que, além de utilizar a Aparência Canônica
Normalizada da Face (\glsxtrlong{capp}--\glsxtrshort{capp}), que é extraída a
partir de um \glsxtrshort{aam}, foi feito um pós-processamento utilizando um
filtro Log-Normal. A aplicação desse filtro faz com que sejam destacadas as
regiões de alta amplitude de energia na imagem. Essas regiões são as que
ocorrem as micro expressões características da dor, como, a \glsxtrshort{au}9.

% roy -> 82% Acc
Ainda utilizando a base de dados McMaster-UNBC, o trabalho visto em
\textcite{roy2016approach} também propõe uma forma diferenciada, para a
extração dos parâmetros da imagem, do que já foi visto até agora. Neste
trabalho o autor utiliza um Filtro de Gabor ou Gabor \textit{Kernel}
\cite{lyons1998codingGabor_1,weldon1996efficientGabor} para a extração de
parâmetros, e então, aplica uma compressão desses parâmetros utilizando a
técnica de Análise de Componentes Principais
(\glsxtrlong{pca}--\glsxtrshort{pca}). Após esse pré-processamento, os dados
são utilizados para o treinamento de uma \glsxtrshort{svm} para classificação
de presença de dor (dor/não-dor) e também para estimação da intensidade da dor
de acordo com a escala \glsxtrshort{pspi}.  Assim como em
\textcite{hammal2012automatic}, a escala foi agrupada em quatro classes, como
já mostrado anteriormente. Os resultados obtidos pelo autor demonstram uma
acurácia\footnote{É a razão entre classificações corretas e o total de
amostras} média de mais de $95\%$ para classificação de dor/não-dor, e de mais
de $80\%$ para classificação da intensidade da dor.

Até o momento, foram apresentadas diversas técnicas, porém, todas utilizam o
paradigma de aprendizado supervisionado, isto é, são fornecidos os dados de
entrada e qual a resposta esperada para tal entrada, para que o algoritmo
aprenda a mapear a entrada para a saída. No entanto, o processo de atribuir
uma resposta correta para os dados de entrada é muito custoso, por exemplo, no
caso da base de dados McMaster-UNBC foi necessário o uso de diversos
profissionais extremamente qualificados para a detecção das \glsxtrshort{au}s e
suas respectivas intensidades. Com isso em mente, o trabalho feito por
\textcite{zhao2016facial} propõe um novo algoritmo capaz de realizar a detecção
da intensidade de dor, tanto de forma supervisionada, quanto não
supervisionada. O algoritmo parte da premissa que as expressões faciais possuem
uma certa ordem cronológica de acontecimento, isto é, existe um momento de
expressão neutra (denominado pelo autor de \textit{onset}), o momento de pico
da expressão (\textit{peak}) e então um momento de expressão neutra
(\textit{offset}). A partir disto, o autor propõe uma combinação entre o
algoritmo de \glsxtrlong{svr} (\glsxtrshort{svr}), que é uma adaptação do
\glsxtrshort{svm} para resolver problemas de regressão, e o algoritmo
\glsxtrlong{or} (\glsxtrshort{or}), que é um algoritmo de aprendizado não
supervisionado onde cada dado é uma variável ordinal, isto é, possui uma ordem
de acontecimento \cite{zhao2016facial}.

A combinação destes dois algoritmos em conjunto com a premissa estabelecida faz
com que seja possível estimar a intensidade de dor mesmo sem possuir os dados
de intensidade como resposta desejada. Os resultados obtidos em
\textcite{zhao2016facial} demonstram uma boa capacidade do algoritmo em estimar
a intensidade de dor, principalmente no caso de aprendizado fracamente
supervisionado, que é o caso em que são fornecidas as respostas desejadas em
apenas alguns exemplos de treinamento, mais especificamente, foram fornecidas
as respostas somente para $8.8\%$ do total de dados utilizados no treinamento.

% rodriguez -> 90% Acc 91.3% AUC
Ainda na área de reconhecimento direto da dor, agora serão apresentados alguns
trabalhos que utilizam a técnica de  Aprendizado Profundo
(\glsxtrlong{dl}--\glsxtrshort{dl}) para a detecção da dor e/ou sua
intensidade.  De acordo com \textcite{rodriguez2017deep}, as técnicas de
\glsxtrshort{dl} têm demonstrado grande capacidade de resolver problemas
complexos de identificação de padrões, como, reconhecimento de ações humanas,
reconhecimento de escrita cursiva ou detecção de faces. A grande vantagem de
uma rede neural profunda é que ela possui a capacidade de extrair, de forma
automática, as características importantes para realizar a discriminação dos
Dados, e também é capaz de combinar estas características para fornecer o
resultado correto \cite{rodriguez2017deep}. Tal capacidade é muito diferente
das técnicas apresentadas até então, onde, as características eram extraídas
manualmente, por exemplo, utilizando \glsxtrshort{lbp} ou uma
\glsxtrshort{dct}.

Existem diversos pontos interessantes que foram explorados no trabalho feito
por \textcite{rodriguez2017deep}, um deles foi realizar o balanceamento dos
dados para treinamento do algoritmo. Esse passo é importante, pois, os dados da
base McMaster-UNBC, em sua grande maioria, são compostos por \textit{frames}
classificados com intensidade nula de dor, para ser mais específico, existem
cerca de 8 mil \textit{frames} classificados com intensidade de dor acima de
zero, e cerca de 40 mil \textit{frames} com intensidade de dor nula, em outras
palavras, os dados são muito esparsos. Esse desbalanceamento de dados pode
fazer com que os modelos tenham a tendência de aprender a detectar a ausência
da dor em detrimento da presença dela, pois, isso resulta numa melhor acurácia
(dada a grande quantidade de amostras com ausência de dor). Para resolver isso,
\textcite{rodriguez2017deep} adotou a técnica de realizar uma sub amostragem
aleatória da classe dominante, ou seja, os dados classificados como não
apresentando dor eram excluídos aleatoriamente, de forma que, a quantidade de
dados de dor e não dor fosse a mesma. Além disso, para melhorar a generalização
do algoritmo, foi utilizada a técnica de \textit{Data Augmentation}, que
consiste em aumentar o volume de dados utilizados no treinamento. Isso é feito
causando deformações aleatórias nas imagens de treinamento, dessa forma, o
conjunto de dados será composto pelas imagens originais e pelas imagens
destorcidas.

Agora falando sobre a detecção de dor em si, \textcite{rodriguez2017deep}
realizou a detecção apenas da presença da dor, e não sua intensidade, ou seja,
um classificador binário. Para isso, foi utilizado um modelo pré-treinado para
extração das características da imagem, esse modelo é denominado VGG\_Faces.
Após a extração das características utilizando este modelo, uma Rede Neural
Convolucional (\glsxtrlong{cnn}--\glsxtrshort{cnn}) foi usada nas
características extraídas, e então, essa rede foi conectada à uma Memória de
Longo Prazo (\glsxtrlong{lstm}--\glsxtrshort{lstm}) para realizar uma análise
temporal das características. A \glsxtrshort{lstm} é uma rede neural
recorrente, que, dada sua arquitetura, é capaz de representar uma relação
temporal entre os dados fornecidos \cite{rodriguez2017deep}. As técnicas
adotadas neste trabalho resultaram num índice AUC de $93.3\%$ para detecção de
dor na base de dados McMaster-UNBC.

%TODO: Apêndice com a definição da AUC ?

% Melhor
Embora os resultados apresentados no trabalho de \textcite{rodriguez2017deep}
sejam bastante expressivos e apontam uma boa acurácia na detecção de dor, a
classificação binária não é muito útil no sistema desenvolvido neste
trabalho de conclusão de curso, pois, a intenção é detectar variações na
intensidade de dor. Porém, \textcite{rodriguez2017deep} demonstra que, a
inclusão da análise temporal realmente possui um grande impacto na detecção de
dor. Com isso em mente, um outro trabalho que também utilizou redes neurais
para detecção de dor pode ser visto em \textcite{zhou2016recurrent}. Nesse
trabalho, o autor utiliza uma Rede Neural Convolucional Recorrente
(\glsxtrlong{rcnn}--\glsxtrshort{rcnn}). À grosso modo, essa arquitetura de
rede neural opera de forma similar ao que foi utilizado em
\textcite{rodriguez2017deep}, no sentido de que a rede é capaz de aprender
relações temporais entre os dados de entrada. A \glsxtrshort{rcnn} foi
construída de forma que os dados de entrada fossem uma sequência de
\textit{frames}, e então, é feita a regressão para determinar a intensidade de
dor daquela sequência de \textit{frames}. De acordo com
\textcite{zhou2016recurrent}, esse tipo de rede neural é comumente aplicado em
problemas de classificação, porém, neste caso deseja-se que a rede realize uma
regressão, isto é, que ela estime valores contínuos para a intensidade de dor.
Para alcançar este resultado, a função de ativação da camada de saída foi
determinada como sendo uma função linear, dessa forma, a intensidade de dor
será proporcional à combinação ponderada do vetor de características.

Os resultados obtidos por \textcite{zhou2016recurrent} são bastante
promissores, é possível notar que o algoritmo pode estimar a intensidade de dor,
com grande fidelidade, os valores reais. Além disso, um ponto
importante para notar é que, como a rede neural analisa os \textit{frames} de
forma dinâmica, ela pode aprender a diferença entre o piscar de olhos e a
contração dos músculos oculares quando em situação de dor, ao passo que, como
dito anteriormente, os algoritmos baseados em análises estáticas não são
capazes de perceber tal diferença, fazendo com que existam picos de intensidade
de dor quando o paciente pisca os olhos.

Em conclusão, analisando os resultados obtidos na literatura, as técnicas que
utilizam redes neurais e aprendizado profundo são capazes de atingir resultados
superiores em relação às outras técnicas de \glsxtrshort{ml}, como a
\glsxtrshort{svm}, tanto em acurácia, quanto em robustez. Porém, estes modelos
tendem a possuir um custo computacional mais elevado, e, demandam um
conhecimento teórico maior para que a implementação seja feita corretamente.
Com isso em mente, a decisão final foi a de utilizar os parâmetros vistos em
\textcite{kaltwang2012continuous} como dados de entrada para modelos
\glsxtrlong{mlp} (\glsxtrshort{mlp}) \cite{russell2002artificial}, os detalhes
da implementação podem ser vistos na Seção \ref{sec:metodologia}.
